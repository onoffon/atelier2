//
//  ModeSelection.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import UIKit

// Actions to handle from the instruments menu
protocol ModeSelectionDelegate: class {
    func setMode(mode: PlayMode)
    func getMode()->PlayMode
    func setInstrument(instrument: Instrument)
    func getInstrument()->Instrument
    func showLinkSettings(sender: UIButton)
    func launchTutorial()
}

import UIKit

@IBDesignable

class ModeSelectionView: UIView {
    
    weak var delegate: ModeSelectionDelegate?
    
    /// the size class for the hight dimension. Compact on iphone/landscape. Regular on iPad/landscape.
    var sizeClass: UIUserInterfaceSizeClass = .regular
    var headerSize: CGFloat = 80
    var title: UILabelTitle!
    var violinButton: UIButton!
    var continuoButton: UIButton!
    var linkButton: UIButton!
    var buttonSize: CGFloat!
    var buttonsWidth: CGFloat!
    var buttonsLeftAlign: CGFloat!
    var width: CGFloat!
    var height: CGFloat!
    var violinImage_off: UIImage!
    var violinImage_on: UIImage!
    var soloModeButton: UIButton!
    var bandModeButton: UIButton!
    var tutorialButton: UIButton!

    
    init(frame: CGRect, sizeClass: UIUserInterfaceSizeClass? = .regular) {
        super.init(frame: frame);
        if let heightClass = sizeClass {
            self.sizeClass = heightClass
        }
        self.customInit();
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.customInit();
    }
    
    // only called within xcode's interface builder. Useful for designable.
    override func prepareForInterfaceBuilder() {
        customInit() 
    }
    
    func customInit() {
        self.backgroundColor = UIColor.white
        width = frame.size.width
        height = frame.size.height
        headerSize = height / 6
        
        let dummyTempFrame = CGRect(x: 0, y: 0, width: 100, height: 100) // real layout will happen in performLayout()
        let dummyTempRadius: CGFloat = 20.0
        title = UILabelTitle(frame: dummyTempFrame)
        title.text = NSLocalizedString("INSTRUMENTS", comment: "titre de page laterale")
        title.textColor = UIColor.black
        title.setUnderlinerColor(color: UIColor.black)
        
        violinImage_off = UIImage(named: "icone_violon_inactif")
        violinImage_on = UIImage(named: "icone_violon_actif")
        violinButton = UIButton()
        violinButton.frame = dummyTempFrame
        violinButton.setBackgroundImage(violinImage_off, for: .normal)
        violinButton.setBackgroundImage(violinImage_on, for: .selected)
        violinButton.layer.cornerRadius = dummyTempRadius
        violinButton.clipsToBounds = true
        violinButton.addTarget(self, action: #selector(ModeSelectionView.pickedViolin(_:)), for: .touchDown)
        
        
        let continuoImage_off = UIImage(named: "icone_continuo_inactif")
        let continuoImage_on = UIImage(named: "icone_continuo_actif")
        continuoButton = UIButton()
        continuoButton.frame = dummyTempFrame
        continuoButton.setBackgroundImage(continuoImage_off, for: .normal)
        continuoButton.setBackgroundImage(continuoImage_on, for: .selected)
        continuoButton.layer.cornerRadius = dummyTempRadius
        continuoButton.clipsToBounds = true
        
        continuoButton.addTarget(self, action: #selector(ModeSelectionView.pickedContinuo(_:)), for: .touchDown)

        soloModeButton = UIButton()
        soloModeButton.frame = dummyTempFrame
        soloModeButton.layer.cornerRadius = dummyTempRadius
        soloModeButton.clipsToBounds = true
        soloModeButton.addTarget(self, action: #selector(ModeSelectionView.toggleMode(_:)), for: .touchDown)
        soloModeButton.setTitle(NSLocalizedString("JOUER À UNE\nTABLETTE", comment: "Titre de bouton"), for:.normal)
        soloModeButton.setTitleColor(UIColor.white, for: .selected)
        soloModeButton.setTitleColor(UIColor(hexString:red), for: .normal)
        
        bandModeButton = UIButton()
        bandModeButton.frame = dummyTempFrame
        bandModeButton.layer.cornerRadius = dummyTempRadius
        bandModeButton.clipsToBounds = true
        bandModeButton.addTarget(self, action: #selector(ModeSelectionView.toggleMode(_:)), for: .touchDown)
        bandModeButton.setTitle(NSLocalizedString("JOUER À PLUSIEURS TABLETTES", comment: "Titre de bouton"), for:.normal)
        bandModeButton.setTitleColor(UIColor.white, for: .selected)
        bandModeButton.setTitleColor(UIColor(hexString:red), for: .normal)
        
        tutorialButton = UIButton()
        tutorialButton.frame = dummyTempFrame
        tutorialButton.clipsToBounds = true
        tutorialButton.addTarget(self, action: #selector(ModeSelectionView.showTutorial(_:)), for: .touchDown)
        tutorialButton.setTitle("?", for: .normal)
        tutorialButton.setTitleColor(.white, for: .normal)
        
        
        let linkButtonImage = UIImage(named: "Ableton_Link_Button_enabled")
        linkButton = UIButton()
        linkButton.frame = dummyTempFrame
        linkButton.setBackgroundImage(linkButtonImage, for: UIControlState())
        linkButton.layer.cornerRadius = dummyTempRadius
        linkButton.clipsToBounds = true
        linkButton.addTarget(self, action: #selector(ModeSelectionView.showLinkSettings(_:)), for: .touchDown)

        self.addSubview(violinButton)
        self.addSubview(continuoButton)
        self.addSubview(linkButton)
        self.addSubview(soloModeButton)
        self.addSubview(bandModeButton)
        self.add(tutorialButton)
        self.addSubview(title)
        
    }
    
    /// Called after view is loaded with view size updated from autolayout constraints.
    override func layoutSubviews() {
        
        switch self.sizeClass {
        case .compact:
            performLayout_CompactHeight()
        case .regular:
            performLayout()
        case .unspecified:
            performLayout()
        }
    }
    
    func performLayout(){
        width = bounds.size.width
        height = frame.size.height
        headerSize = height / 6
        buttonSize = width / 2
        buttonsLeftAlign = (width - buttonSize) / 2
        
        soloModeButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize , width: buttonSize * 1.8, height: buttonSize / 2)
        soloModeButton.center.x = width / 2
        soloModeButton.layer.borderWidth = 4
        soloModeButton.layer.borderColor = UIColor(hexString: red).cgColor
        soloModeButton.layer.cornerRadius = soloModeButton.frame.size.height / 2
        soloModeButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: 20)
        soloModeButton.titleLabel?.numberOfLines = 2
        soloModeButton.titleLabel?.textAlignment = .center
        soloModeButton.setBackgroundColor(color: UIColor(hexString: red), forState: .selected)
        soloModeButton.setBackgroundColor(color: .white, forState: .normal)
        
        bandModeButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize + buttonSize * 0.75, width: buttonSize * 1.8, height: buttonSize / 2)
        bandModeButton.center.x = width / 2
        bandModeButton.layer.borderWidth = 4
        bandModeButton.layer.borderColor = UIColor(hexString: red).cgColor
        bandModeButton.layer.cornerRadius = soloModeButton.frame.size.height / 2
        bandModeButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: 20)
        bandModeButton.titleLabel?.numberOfLines = 2
        bandModeButton.titleLabel?.textAlignment = .center
        bandModeButton.setBackgroundColor(color: UIColor(hexString: red), forState: .selected)
        bandModeButton.setBackgroundColor(color: .white, forState: .normal)

        violinButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + buttonSize * 1.5, width: buttonSize, height: buttonSize)
        violinButton.layer.cornerRadius = buttonSize / 2
        continuoButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + buttonSize * 2.75, width: buttonSize, height: buttonSize)
        continuoButton.layer.cornerRadius = buttonSize / 2
        
        tutorialButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize + buttonSize * 4.2 , width: buttonSize / 2, height: buttonSize / 2)
        tutorialButton.center.x = width / 2
        tutorialButton.layer.cornerRadius = buttonSize / 4
        tutorialButton.titleLabel?.font = UIFont(name: "Frutiger-Roman", size: 20)
        tutorialButton.titleLabel?.textAlignment = .center
        tutorialButton.setBackgroundColor(color: UIColor(hexString:red), forState: .normal)
        
        linkButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + buttonSize * 4.2, width: buttonSize/2, height: buttonSize/2)
        linkButton.isHidden = true
        linkButton.center.x = width / 3
        
        title.frame = CGRect(x: 0, y: headerSize / 4, width: width / 3, height: headerSize/4)
        title.adjustsFontSizeToFitWidth = true
        title.center.x = width / 2
        
        // init ui from mode
        let currentMode: PlayMode = (delegate?.getMode())!

    }

    func performLayout_CompactHeight(){
        width = bounds.size.width
        height = frame.size.height
        headerSize = height / 6
        
        let heightButtonsEstate = height - headerSize
        
        /// based on some raw drawing , I can snap the buttons layout according to a 17 bins grid
        let binHeight = heightButtonsEstate / 17
        
        buttonSize = width / 2
    
        buttonsLeftAlign = width / 4
    
        let modeButtonsHeight = binHeight * 5 / 3
        
        soloModeButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize , width: width * 0.9, height: modeButtonsHeight)
        soloModeButton.center.x = width / 2
        soloModeButton.layer.borderWidth = 2
        soloModeButton.layer.borderColor = UIColor(hexString: red).cgColor
        soloModeButton.layer.cornerRadius = soloModeButton.frame.size.height / 2
        soloModeButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: 13)
        soloModeButton.titleLabel?.numberOfLines = 1
        soloModeButton.setTitle(NSLocalizedString("JOUER À UNE TABLETTE", comment: "Titre de bouton"), for:.normal)
        soloModeButton.titleLabel?.textAlignment = .center
        soloModeButton.setBackgroundColor(color: UIColor(hexString: red), forState: .selected)
        soloModeButton.setBackgroundColor(color: .white, forState: .normal)
        
        bandModeButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize + binHeight * 3, width: width * 0.9, height: modeButtonsHeight)
        bandModeButton.center.x = width / 2
        bandModeButton.layer.borderWidth = 2
        bandModeButton.layer.borderColor = UIColor(hexString: red).cgColor
        bandModeButton.layer.cornerRadius = bandModeButton.frame.size.height / 2
        bandModeButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: 13)
        bandModeButton.setTitle(NSLocalizedString("JOUER À PLUSIEURS TABLETTES", comment: "Titre de bouton"), for:.normal)
        bandModeButton.titleLabel?.numberOfLines = 1
        bandModeButton.titleLabel?.textAlignment = .center
        bandModeButton.setBackgroundColor(color: UIColor(hexString: red), forState: .selected)
        bandModeButton.setBackgroundColor(color: .white, forState: .normal)
        
        
        violinButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + binHeight * 6, width: binHeight * 5, height: binHeight * 5)
        violinButton.layer.cornerRadius = binHeight * 3 / 2
        violinButton.center.x = width / 4
        continuoButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + binHeight * 6, width: binHeight * 5, height: binHeight * 5)
        continuoButton.layer.cornerRadius = binHeight * 3 / 2
        continuoButton.center.x = width * 3 / 4
        
        tutorialButton.frame = CGRect(x: buttonsLeftAlign / 2, y: headerSize + binHeight * 12 , width: binHeight * 3, height: binHeight * 3)
        tutorialButton.center.x = width / 2
        tutorialButton.layer.cornerRadius = binHeight * 3 / 2
        tutorialButton.titleLabel?.font = UIFont(name: "Frutiger-Roman", size: 15)
        tutorialButton.titleLabel?.textAlignment = .center
        tutorialButton.setBackgroundColor(color: UIColor(hexString:red), forState: .normal)
        
        linkButton.frame = CGRect(x: buttonsLeftAlign, y: headerSize + binHeight * 12, width: binHeight * 3, height: binHeight * 3)
        linkButton.isHidden = true
        linkButton.center.x = width / 3
        
        title.frame = CGRect(x: 0, y: headerSize / 4, width: width / 3, height: headerSize/4)
        title.adjustsFontSizeToFitWidth = true
        title.center.x = width / 2
        
        // init ui from mode
        let currentMode: PlayMode = (delegate?.getMode())!
        
    }
    
    
    //MARK: button actions
    func pickedViolin(_ sender: UIButton){
        
        if (delegate?.getMode() == PlayMode.multiplePlayers){
            violinButton.isSelected = true
            continuoButton.isSelected = false
            delegate?.setInstrument(instrument: Instrument.solo)
        } else {
            violinButton.isSelected = true
        }
    }
    
    func pickedContinuo(_ sender: UIButton){
        
        if (delegate?.getMode() == PlayMode.multiplePlayers){
            continuoButton.isSelected = true
            violinButton.isSelected = false
            delegate?.setInstrument(instrument: Instrument.continuo)
        } else {
            continuoButton.isSelected = true
        }
    }
    
    func showLinkSettings(_ sender: UIButton){
        delegate?.showLinkSettings(sender: sender)
    }
    
    func toggleMode(_ sender: UIButton){
        
        if !sender.isSelected {
           let currentMode: PlayMode = (delegate?.getMode())!
            let newMode: PlayMode
            switch currentMode {
            case PlayMode.multiplePlayers:
                newMode = PlayMode.onePlayer
            case PlayMode.onePlayer:
                newMode = PlayMode.multiplePlayers
            }
            modeUpdateControls(forMode: newMode)
        }
    }
    
    func modeUpdateControls(forMode: PlayMode){

        switch forMode {
        case PlayMode.onePlayer:
            // set to one player
            soloModeButton.isSelected = true
            bandModeButton.isSelected = false
            violinButton.isSelected = true
            continuoButton.isSelected = true
            delegate?.setMode(mode: PlayMode.onePlayer)
            linkButton.isHidden = true
            tutorialButton.center.x = width / 2
            
        case PlayMode.multiplePlayers:
            // set to multiple players
            soloModeButton.isSelected = false
            bandModeButton.isSelected = true
            delegate?.setMode(mode: PlayMode.multiplePlayers)
            continuoButton.isSelected = false
            violinButton.isSelected = true
            
            switch sizeClass{
            case .compact:
                tutorialButton.center.x = continuoButton.center.x
                linkButton.center.x = violinButton.center.x
            default:
                tutorialButton.center.x = width * 2 / 3
                linkButton.center.x = width / 3
            }

            delegate?.setInstrument(instrument: Instrument.solo)            
            linkButton.isHidden = false
        }
    }

    func showTutorial(_ sender: UIButton){
        delegate?.launchTutorial()
    }

}
