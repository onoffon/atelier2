//
//  VariationsPaletteView.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//


import UIKit
import Foundation

protocol VariationPaletteDelegate: class {
    func selectedVariation(_ tag: Int)
    func longPressedVariation(_ tag: Int)
    func getFooterHeight()->(CGFloat)
    func selectedVariationPart(_ index: Int)
    func soloVariationPart(_ index: Int)
    func unsoloVariationParts()
}

/**
 Model for a variation as displayed in the palette view.
 */
struct VariationPaletteModel{
    /// the part label displayed on top of the palette - used for polyphonic continuo.
    var title: String
    /// the variation labels.
    var variationNames: [String]
}

class VariationPaletteView: UIView {
    var headerSize: CGFloat = 80
    var footerSize: CGFloat = 15
    
    var variationButtons:[[UIButton]] = [[]]
    var partSelectionButtons: [UIButton] = []
    var partSelectionButtonWidth: CGFloat!
    var partSelectionButtonHeight: CGFloat!
    var numberOfRows: Int!
    var buttonSlotWidth: CGFloat!
    @IBInspectable var buttonSlotHeight: CGFloat!
    var width: CGFloat = 400 // dummy
    var height: CGFloat = 400 // dummy
    
    var title: UILabelTitle =  UILabelTitle(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) // dummy
    var numberOfVariationsInSelectedPart: Int!
    weak var delegate: VariationPaletteDelegate?
    var labelFontSize =  CGFloat(20)
    var labelFontSizeHighlight = CGFloat(40)
    var heightClass: UIUserInterfaceSizeClass = .regular {
        didSet{
            switch heightClass {
            case .compact:
                self.labelFontSize = CGFloat(12)
                self.labelFontSizeHighlight = CGFloat(25)
            default:
                self.labelFontSize = CGFloat(20)
                self.labelFontSizeHighlight = CGFloat(40)
            }
        }
    }
    

    
    var variationNames: [String] = defaultVariationNames {
        didSet{
            for (i, name) in variationNames.enumerated(){
                if (name != oldValue[i]){
                    refreshPaletteName(index: i)
                }
            }
        }
    }
    /// the current loaded palette variations model
    var currentVariationsModel: [VariationPaletteModel]!
    
    /// number of parts > 1 if polyphonic. defaults to 1.
    var numberOfParts: Int = 1
    
    /// the selecter part the user is editing - used for polyphonic continuo. defaults to 0.
    var selectedPartIndex = 0 {
        didSet{
            togglePartButtons(index: selectedPartIndex)
            toggleVariationButtons(index: selectedPartIndex)
        }
    }
    
    init(frame: CGRect, numberOfVariations: Int, sizeClass: UIUserInterfaceSizeClass? = .regular){
        
        super.init(frame:frame)
        if let size = sizeClass{
            self.heightClass = size
        }
        //        self.customInit(variationNames: Array(self.variationNames[0...(numberOfVariations-1)]))
        
    }
    
    
    init(frame: CGRect, variations: [VariationPaletteModel], sizeClass: UIUserInterfaceSizeClass? = .regular){

        super.init(frame:frame)
        
        if let size = sizeClass{
            self.heightClass = size
        }
        self.variationNames = variations[selectedPartIndex].variationNames
        self.currentVariationsModel = variations
        self.customInit(variations: self.currentVariationsModel, sizeClass: self.heightClass)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        //        self.customInit(variationNames: defaultVariationNames);
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        //        self.customInit(variationNames: defaultVariationNames);
    }
    
    func customInit(variations: [VariationPaletteModel], sizeClass: UIUserInterfaceSizeClass? = .regular) {
        if let size = sizeClass{
            self.heightClass = size
        }
        
        numberOfParts = variations.count
        
        self.numberOfVariationsInSelectedPart = variations[selectedPartIndex].variationNames.count
        height = frame.size.height
        headerSize = height / 6
        self.backgroundColor = UIColor.white
        width = frame.size.width
        height = frame.size.height
        buttonSlotWidth = width
        buttonSlotHeight = (height - headerSize - footerSize) / CGFloat(numberOfVariationsInSelectedPart!)
        
        title = UILabelTitle(frame: CGRect(x: 0, y: headerSize / 4, width: width/3, height: headerSize/4))
        title.text = "VARIATIONS"
        title.textColor = UIColor.black
        title.setUnderlinerColor(color: UIColor.black)
        self.addSubview(title)
        
        // add part selection buttons in case continuo is polyphonic
        if numberOfParts > 1 {
            let y = 2 * headerSize / 3
            partSelectionButtonHeight = headerSize - y
            partSelectionButtonWidth = partSelectionButtonHeight
            let buttonHalfSize = partSelectionButtonWidth / 2
            for (i, part) in variations.enumerated() {
                let partSelectionButton = UIButton()
//                let partTitle = part.title
//                let index = partTitle.index(partTitle.startIndex, offsetBy: 2)
//                let partShortName = part.title.substring(to: index)
                partSelectionButton.setTitle(part.title, for: UIControlState())
                let x = (CGFloat(i + 1 ) * width / CGFloat(numberOfParts + 1)) - buttonHalfSize
                
                partSelectionButton.frame = CGRect(x: x, y: y - buttonHalfSize / 2 , width: partSelectionButtonWidth, height: partSelectionButtonHeight)
                var color: UIColor
                if i == selectedPartIndex {
                    color = UIColor(hexString: candycane[10]) // yellow
                } else {
                    color = UIColor(hexString: lightGrey)
                }
                partSelectionButton.backgroundColor = color
                partSelectionButton.layer.cornerRadius = partSelectionButton.frame.width / 2
                partSelectionButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: labelFontSize)
                partSelectionButton.tag = i
                partSelectionButton.addTarget(self, action: #selector(VariationPaletteView.partSelectionButtonTouched), for: .touchUpInside)
                let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(VariationPaletteView.partSelectionButtonLongPress(_:)))
                partSelectionButton.addGestureRecognizer(longPressGesture)
                partSelectionButtons.append(partSelectionButton)
                self.addSubview(partSelectionButton)
            }
        }
        
        for partIndex in 0...(numberOfParts - 1){
            variationButtons.append([])
            let numberOfRowsInPart = variations[partIndex].variationNames.count
            var tag = -1
            for i in 0..<numberOfRowsInPart{
                let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(VariationPaletteView.variationButtonLongPress(_:)))
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(VariationPaletteView.variationButtonTouched(_:)))
                tag += 1
                let variationButton = UIButton()
                variationButton.frame = CGRect(x: 0, y: CGFloat(i) * buttonSlotHeight + headerSize, width: buttonSlotWidth, height: buttonSlotHeight)
                variationButton.tag = tag
                variationButton.setTitle("\(variations[partIndex].variationNames[tag])", for: UIControlState())
                variationButton.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: labelFontSize)
                let color: UIColor = UIColor(hexString: candycane[tag])
                variationButton.backgroundColor = color
                variationButton.addGestureRecognizer(tapGesture)
                variationButton.addGestureRecognizer(longPressGesture)
                
                if (partIndex == self.selectedPartIndex) {
                    variationButton.isHidden = false
                } else {
                    variationButton.isHidden = true
                }
                
                variationButtons[partIndex].append(variationButton)
                
                
                self.addSubview(variationButton)
            }
        }
    }
    
    
    // Called after view is loaded with view size updated from autolayout constraints.
    override func layoutSubviews() {
        
        buttonSlotWidth = bounds.size.width
        footerSize = (delegate?.getFooterHeight())!
        headerSize = height / 6
        width = frame.size.width
        height = frame.size.height
        
        for partIndex in 0...(self.numberOfParts - 1){
            let numberOfVariationsInPart = variationButtons[partIndex].count
            
            buttonSlotHeight = (bounds.size.height - headerSize - footerSize) / CGFloat(numberOfVariationsInPart)

            for (i, button) in variationButtons[partIndex].enumerated(){
                button.frame = CGRect(x: 0, y: CGFloat(i) * buttonSlotHeight + headerSize, width: buttonSlotWidth, height: buttonSlotHeight)
                button.titleLabel?.numberOfLines = 1
                button.titleLabel?.adjustsFontSizeToFitWidth = true
            }
        }
        
        title.frame = CGRect(x: 0, y: headerSize / 4, width: width / 3, height: headerSize/4)
        title.adjustsFontSizeToFitWidth = true
        title.center.x = width / 2
        
    }
    
    //MARK: input from controller
    
    func highlightVariation(index: Int){
        for (i, button) in variationButtons[selectedPartIndex].enumerated(){
            if (i == index){
                button.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: labelFontSizeHighlight)
            } else{
                button.titleLabel!.font =  UIFont(name: "Frutiger-Roman", size: labelFontSize)
            }
        }
    }
    
    func refreshPaletteName(index: Int){
        variationButtons[selectedPartIndex][index].setTitle("\(variationNames[index])", for: UIControlState())
    }
    
    
    //MARK: output to controller
    func variationButtonTouched(_ sender: UIGestureRecognizer){
        print("touched")
        highlightVariation(index: (sender.view?.tag)!)
        delegate?.selectedVariation((sender.view?.tag)!)
    }
    
    
    func variationButtonLongPress(_ sender: UIGestureRecognizer){
        if sender.state == .began {
            print("long press \(sender.view?.tag)")
            highlightVariation(index: (sender.view?.tag)!)
            delegate?.longPressedVariation((sender.view?.tag)!)
        }
    }
    
    func partSelectionButtonLongPress(_ sender: UIGestureRecognizer){
        if sender.state == .began {
            let partIndex = (sender.view?.tag)!
            if (self.selectedPartIndex != partIndex) {
                self.partSelectionButtonTouched(partSelectionButtons[partIndex])
            }
            markSoloPartButton(index: partIndex)
        }
    }
    
    func partSelectionButtonTouched(_ sender: UIButton){
        print("touched button with tag \(sender.tag)")
        delegate?.unsoloVariationParts()
        self.selectedPartIndex = sender.tag
        delegate?.selectedVariationPart(sender.tag)
        highlightVariation(index: 0)
        delegate?.selectedVariation(0)
    }
    
    //MARK- helpers
    func togglePartButtons(index: Int){
        var color: UIColor
        for (i, button) in partSelectionButtons.enumerated() {
            if i == index {
                color = UIColor(hexString: candycane[10]) //yellow
            } else {
                color = UIColor(hexString: lightGrey)
            }
            button.backgroundColor = color
        }
    }
    
    //MARK- helpers
    
    /// Solo a continuo part
    func markSoloPartButton(index: Int){
        var color: UIColor
        for (i, button) in partSelectionButtons.enumerated() {
            if i == index {
                color = UIColor(hexString: candycane[8]) //red-ish
                delegate?.soloVariationPart(index)
            } else {
                color = UIColor(hexString: lightGrey)
            }
            button.backgroundColor = color
        }
    }
    
    func toggleVariationButtons(index: Int) {
        var color: UIColor
        for partIndex in 0...(self.numberOfParts - 1){
            for button in variationButtons[partIndex] {
                if partIndex == index {
                    button.isHidden = false
                } else {
                    button.isHidden = true
                }
            }
        }
    }
    
    //    // Only override drawRect: if you perform custom drawing.
    //    // An empty implementation adversely affects performance during animation.
    //    override func drawRect(rect: CGRect) {
    //        // Drawing code
    //
    //
    //    }
    
}
