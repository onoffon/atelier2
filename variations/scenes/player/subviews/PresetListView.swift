//
//  PresetListView.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import UIKit
import Foundation

protocol PresetViewDelegate: class {
    func recall(_ tag: Int)
    func trashButtonPushed()
}


@IBDesignable
class PresetListView: UIView {
    
    var headerSize: CGFloat = 80
    var buttonColor_selected: UIColor = UIColor(hexString: red)
    var buttonColor_unselected: UIColor = UIColor(hexString: lightGrey)
    var selectedPreset: Int = 0
    var presetButtons:[UIButton] = []
    var numberOfRows: Int!
    var numberOfCols: Int!
    var buttonSlotSize: CGFloat!
    var width: CGFloat!
    var height: CGFloat!
    
    var trashButton: UIButton!
    var title: UILabelTitle!
    weak var delegate: PresetViewDelegate?
    
    // Mark: - life cycle
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.customInit();
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.customInit();
        
    }
    
    // only called within xcode's interface builder. Useful for designable.
    override func prepareForInterfaceBuilder() {
        customInit()
    }
    
    func customInit() {
        
        self.backgroundColor = UIColor.white
        width = frame.size.width
        height = frame.size.height
        headerSize = height / 6
        
        
        numberOfRows = numberOfPresetSlots
        
        buttonSlotSize = width / 8.0
        
        title = UILabelTitle(frame: CGRect(x: width/8, y: 0, width: width, height: 30))
        title.text = NSLocalizedString("MÉMOIRES", comment: "Preset sidebar title")
        title.textColor = UIColor.black
        title.setUnderlinerColor(color: UIColor.black)
        title.font = UIFont(name: "Frutiger-Roman", size: 20)
        self.addSubview(title)
        
        var tag = -1
        for i in 0..<numberOfRows{
            
            tag += 1
            let presetButton = UIButton()
            
            presetButton.frame = CGRect(x:  buttonSlotSize, y: CGFloat(i) * buttonSlotSize + headerSize, width: buttonSlotSize, height: buttonSlotSize)
            presetButton.tag = tag
            presetButton.setTitle("\(tag + 1)", for: UIControlState())
            presetButton.backgroundColor = buttonColor_unselected
            presetButton.setTitleColor(UIColor.white, for: UIControlState())
            presetButton.titleLabel?.font = UIFont(name: "Frutiger-Bold", size: 20)
            self.addSubview(presetButton)
            presetButton.addTarget(self, action: #selector(PresetListView.presetButtonAction(_:)), for: .touchUpInside)
            presetButtons.append(presetButton)
            
        }
        
        highlightPreset(selectedPreset)
        
        //        trashButton = UIButton()
        //        trashButton.setImage(UIImage(named: "bin"), for: UIControlState())
        //        trashButton.frame=CGRect(x: 0, y: 0, width: buttonSlotSize, height: buttonSlotSize) //dummy, repositioned in layoutSubviews()
        //        trashButton.addTarget(self, action: #selector(PresetListView.trashButtonAction(_:)), for: .touchUpInside)
        //        self.addSubview(trashButton)
        
    }
    
    
    // Called after view is loaded with view size updated from autolayout constraints.
    override func layoutSubviews() {
        width = bounds.size.width
        height = bounds.size.height
        headerSize = height / 6
        
        let interButtonSpace: CGFloat = (height - headerSize) / CGFloat(numberOfPresetSlots  * 2)
        buttonSlotSize = interButtonSpace * 1.5
        
        var buttonIndex:Int = -1
        for i in 0..<numberOfRows{
            
            buttonIndex += 1
            presetButtons[buttonIndex].frame.size.width = buttonSlotSize
            presetButtons[buttonIndex].frame.size.height = buttonSlotSize
            presetButtons[buttonIndex].center = CGPoint(x: width / 2, y: CGFloat(i) * interButtonSpace * 2.0 + headerSize )
            
            presetButtons[buttonIndex].layer.cornerRadius = buttonSlotSize / 2
            
        }
        

        title.frame = CGRect(x: 0, y: headerSize / 4, width: width / 3, height: headerSize/4)
        title.adjustsFontSizeToFitWidth = true
        title.center.x = width / 2
        
        //        trashButton.frame = CGRect(x: title.center.x + title.frame.width, y: 0, width: buttonSlotSize / 3, height: buttonSlotSize / 3)
        //        trashButton.center.y = title.center.y
        
        
    }
    
    // MARK: - events
    
    func highlightPreset(_ index: Int){
        for i in 0 ..< presetButtons.count{
            if i==index{
                presetButtons[i].backgroundColor = buttonColor_selected
            } else {
                presetButtons[i].backgroundColor = buttonColor_unselected
            }
        }
    }
    
    func presetButtonAction(_ sender: UIButton){
        highlightPreset(sender.tag)
        delegate?.recall(sender.tag)
    }
    func trashButtonAction(_ sender: UIButton){
        //        delegate?.trashButtonPushed()
    }
    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    //    override func drawRect(rect: CGRect) {
    //
    //        // Drawing code
    //
    //
    //    }
    
    
}
