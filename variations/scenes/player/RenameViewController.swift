//
//  RenameViewController.swift
//  variations
//
//  Created by Julien Bloit on 4/9/17.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation

class RenameViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var variationTextField: UITextField!
    @IBOutlet weak var variationTitleLabel: UILabel!
    @IBOutlet weak var renameVariationButton: UIButtonTitle!
    @IBOutlet weak var drawVariationButton: UIButtonTitle!
    @IBOutlet weak var patternDrawContainer: UIView!
    @IBOutlet weak var renameFormContainer: UIView!
    @IBOutlet weak var titleBar: UIView!
    @IBOutlet weak var renameDrawLabelsStack: UIStackView!
    @IBOutlet weak var renameFormTopConstraint: NSLayoutConstraint!
    var initConstantConstraint: CGFloat!
    var hasMovedTextField: Bool = false
    
    var lockScreenView: LockScreen!
    var pattern: [[Int]] = [[]] {
        didSet{
            print(" --  set pattern \(pattern)")
        }
    }
    var preset: PartPreset!
    var delegate: RenameControllerDelegate?
    var selectedVariationIndex: Int!
    

    // handle toggle titles
    var isRenameSelected: Bool = true{
        didSet{
            if (isRenameSelected) {
                drawVariationButton.isSelected = false
                renameVariationButton.isSelected = true
                patternDrawContainer.isHidden = true
                renameFormContainer.isHidden = false
            } else {
                drawVariationButton.isSelected = true
                renameVariationButton.isSelected = false
                patternDrawContainer.isHidden = false
                renameFormContainer.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        renameVariationButton.setTitle(NSLocalizedString("RENOMMER", comment: "titre d'onglet dans pop up d'edition de variation"), for: .normal)
        drawVariationButton.setTitle(NSLocalizedString("DESSINER", comment: "titre d'onglet dans pop up d'edition de variation"), for: .normal)
        variationTextField.borderAsGreyUnderline()
        variationTextField.delegate = self

        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        isRenameSelected = true
        displayPartPreset()
//        pattern = preset.variationPatterns[de]
    }
    
    override func viewDidLayoutSubviews() {
        //square angles for view
        self.view.superview?.layer.cornerRadius = 0
        
        // Add the pattern-lock view
        let gridSize = 4
        let size = patternDrawContainer.frame.size.height
        let lockScreenFrame = CGRect(x: 0, y: 0, width: size , height: size)

        // Example of using config
        var config = LockScreen.Config()
        config.lineColor = UIColor(hexString: red)
        
        lockScreenView = LockScreen(frame: lockScreenFrame, size: gridSize, allowClosedPattern: true, config: config) { [weak self] pattern in
            self?.pattern = pattern
            
        }
        patternDrawContainer.addSubview(lockScreenView)
        lockScreenView.center.x = patternDrawContainer.center.x
        
        print("break")
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.superview?.layer.cornerRadius = 0
    }
    
    @IBAction func renameVariationButtonTouched(_ sender: UIButtonTitle){
        self.isRenameSelected = true
    }
    
    @IBAction func drawVariationButtonTouched(_ sender: UIButtonTitle){
        self.isRenameSelected = false
    }
    
    @IBAction func eraseButtonTouched(_ sender: UIButton) {
        lockScreenView.resetScreen()
    }
    
    @IBAction func validatePattern(_ sender: UIButton) {
        
        delegate?.commitNewPattern(newPattern: self.pattern)
        dismiss(animated: false, completion: nil)

    }
    
    @IBAction func dismissButtonTouched(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func validateNewName(_ sender: UIButton){
        delegate?.commitNewName(newName: variationTextField.text!)
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - text field
    
    // when user hits return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification: NSNotification){
        let vc = self.presentingViewController
        
        // If this is an iPhone's interface
        if (vc?.traitCollection.verticalSizeClass == .compact){
            let value: NSValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardSize: CGRect  = value.cgRectValue;
            let maxYPosTextField: CGFloat = self.renameFormContainer.frame.origin.y + self.variationTextField.frame.origin.y + self.variationTextField.frame.size.height;
            
            let bottomSpace = self.view.frame.origin.y + self.view.frame.size.height - maxYPosTextField
            if ( bottomSpace < keyboardSize.size.height){
                self.view.layoutIfNeeded()
                self.initConstantConstraint = self.renameFormTopConstraint.constant
                self.renameFormTopConstraint.constant -= keyboardSize.size.height
                
                self.hasMovedTextField = true
                UIView.animate(withDuration: 0.2, animations: {
                    self.renameDrawLabelsStack.isHidden = true
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        if (self.hasMovedTextField){
            self.view.layoutIfNeeded()
            self.renameFormTopConstraint.constant = self.initConstantConstraint
            UIView.animate(withDuration: 0.2, animations: {
                self.renameDrawLabelsStack.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    func displayPartPreset(){
        self.variationTitleLabel.text = self.preset.variationNames[selectedVariationIndex]
        self.titleBar.backgroundColor = UIColor(hexString:candycane[selectedVariationIndex])
        

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 20
    }
    
    
}
