//
//  SidebarPresenter.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright (c) 2016 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol SidebarPresenterInput
{
  func presentSomething(response: Sidebar.Something.Response)
}

protocol SidebarPresenterOutput: class
{
  func displaySomething(viewModel: Sidebar.Something.ViewModel)
}

class SidebarPresenter: SidebarPresenterInput
{
  weak var output: SidebarPresenterOutput!
  
  // MARK: - Presentation logic
  
  func presentSomething(response: Sidebar.Something.Response)
  {
    // NOTE: Format the response from the Interactor and pass the result back to the View Controller
    
    let viewModel = Sidebar.Something.ViewModel()
    output.displaySomething(viewModel: viewModel)
  }
}
