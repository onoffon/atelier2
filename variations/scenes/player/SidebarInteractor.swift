//
//  SidebarInteractor.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright (c) 2016 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol SidebarInteractorInput
{
  func doSomething(request: Sidebar.Something.Request)
}

protocol SidebarInteractorOutput
{
  func presentSomething(response: Sidebar.Something.Response)
}

class SidebarInteractor: SidebarInteractorInput
{
  var output: SidebarInteractorOutput!
  var worker: SidebarWorker!
  
  // MARK: - Business logic
  
  func doSomething(request: Sidebar.Something.Request)
  {
    // NOTE: Create some Worker to do the work
    
    worker = SidebarWorker()
    worker.doSomeWork()
    
    // NOTE: Pass the result to the Presenter
    
    let response = Sidebar.Something.Response()
    output.presentSomething(response: response)
  }
}
