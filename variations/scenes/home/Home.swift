//
//  Home.swift
//  variations
//
//  Created by julien@macmini on 21/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import UIKit

class Home: UIViewController {

    
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String{
                self.versionLabel.text = "v\(version) (\(buildNumber))"
            } else {
                self.versionLabel.text = "v\(version))"
            }
        }
    }
    
    @IBAction func buttonJouerEnsembleTouched(_ sender: Any) {
        let appURL: URL? = URL(string: "TalensSchoolOrchestre://app")
        let itunesURL: URL? = URL(string: "itms-apps://itunes.apple.com/app/id1233988131")
        launchAppWithURL(appURL: appURL, iTunesURL: itunesURL)
    }
    
    @IBAction func buttonInterpreterTouched(_ sender: Any) {
        let appURL: URL? = URL(string:"TalensSchoolInterpreter://app")
        let itunesURL: URL? = URL(string: "itms-apps://itunes.apple.com/app/id1230873613")
        launchAppWithURL(appURL: appURL, iTunesURL: itunesURL)
    }
    
    func launchAppWithURL(appURL: URL?, iTunesURL: URL?){

        var openedAppUrl = false
        if let url = appURL {
            if (UIApplication.shared.canOpenURL(url)){
                if (UIApplication.shared.openURL(url)){
                    openedAppUrl = true
                }
            } else {
                print("can't open app url")
            }
        }
        if (!openedAppUrl){
            
            if let url = iTunesURL,
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
}
