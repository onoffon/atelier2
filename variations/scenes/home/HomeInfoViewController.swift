//
//  HomeInfoViewController.swift
//  variations
//
//  Created by julien@macmini on 22/02/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

enum Tabs{
    case presentation
    case partenaires
    case credits
}


class HomeInfoViewController: UIViewController {
    
    @IBOutlet weak var presentationText: UITextView!
    @IBOutlet weak var partnersScrollView: UIScrollView!
    var partnersImageView: UIImageView!
    var partnersImageRatio: CGFloat!
    
    
    @IBOutlet weak var creditsText: UITextView!
    
    @IBOutlet weak var presentationButton: UIButtonTitle!
    @IBAction func presentationTouched(_ sender: Any) {
        showTab(tab: .presentation)
    }
    
    @IBOutlet weak var partnersButton: UIButtonTitle!
    @IBAction func partnersTouched(_ sender: Any) {
        showTab(tab: .partenaires)
    }
    
    @IBOutlet weak var creditsButton: UIButtonTitle!
    @IBAction func creditTouched(_ sender: Any) {
        showTab(tab: .credits)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadText(textFile: "presentation", textView: presentationText)
        loadText(textFile: "credits", textView: creditsText)

        // load partners image
        partnersImageView = UIImageView()
        partnersImageView.image = #imageLiteral(resourceName: "partners")
        partnersImageRatio = #imageLiteral(resourceName: "partners").size.width / #imageLiteral(resourceName: "partners").size.height
        partnersImageView.contentMode = .scaleAspectFit
        partnersScrollView.addSubview(partnersImageView)
        
        presentationButton.setUnderlinerColor(color: .white)
        partnersButton.setUnderlinerColor(color: .white)
        creditsButton.setUnderlinerColor(color: .white)

        showTab(tab: .presentation)
    }
    override func viewDidLayoutSubviews() {
        // This part needs to happen after the constraints have been applied (to get scrollview's dimensions)
        partnersImageView.frame = CGRect(x:0, y:0,
                                         width: partnersScrollView.frame.width,
                                         height:  partnersScrollView.frame.width / partnersImageRatio )
        partnersScrollView.contentSize = partnersImageView.frame.size
    }
    
    
    func loadText(textFile: String, textView: UITextView){
        if let rtfURL = Bundle.main.url(forResource: textFile, withExtension: "rtf"){
            var d : NSDictionary? = infoTextAttributes()
            let attributedStringWithRtf = try! NSAttributedString(
                url: rtfURL,
                options: [NSDocumentTypeDocumentAttribute : NSRTFTextDocumentType],
                documentAttributes: &d)
            textView.attributedText = attributedStringWithRtf
        }
    }
    

    
    
    
    func showTab(tab: Tabs){
        switch tab  {
        case .partenaires:
            partnersScrollView.isHidden = false
            partnersButton.border.isHidden = false
            
            presentationText.isHidden = true
            presentationButton.border.isHidden = true
            
            creditsText.isHidden = true
            creditsButton.border.isHidden = true
            
        case .credits:
            partnersScrollView.isHidden = true
            partnersButton.border.isHidden = true
            
            creditsText.isHidden = false
            creditsButton.border.isHidden = false
            
            presentationText.isHidden = true
            presentationButton.border.isHidden = true
            
        case .presentation:
            partnersScrollView.isHidden = true
            partnersButton.border.isHidden = true
            
            creditsText.isHidden = true
            creditsButton.border.isHidden = true
            
            presentationText.isHidden = false
            presentationButton.border.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
    }
    
}
