//
//  ProfileRouter.swift
//  variations
//
//  Created by julien@macmini on 03/12/2016.
//  Copyright (c) 2016 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol ProfileRouterInput
{
    func navigateToStudentsList()
}

class ProfileRouter: ProfileRouterInput
{
    weak var viewController: ProfileViewController!
    
    // MARK: - Navigation
    
    func navigateToStudentsList()
    {
        // NOTE: Teach the router how to navigate to another scene. Some examples follow:
        
        // 1. Trigger a storyboard segue
        viewController.performSegue(withIdentifier: "showStudentList", sender: nil)
        
        // 2. Present another view controller programmatically
        // viewController.presentViewController(someWhereViewController, animated: true, completion: nil)
        
        // 3. Ask the navigation controller to push another view controller onto the stack
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
        
        // 4. Present a view controller from a different storyboard
        // let storyboard = UIStoryboard(name: "OtherThanMain", bundle: nil)
        // let someWhereViewController = storyboard.instantiateInitialViewController() as! SomeWhereViewController
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
    }
    
    // MARK: - Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue)
    {
        // NOTE: Teach the router which scenes it can communicate with
        
        if segue.identifier == "showStudentList" {
//            passDataToStudentsList(segue: segue)
        }
    }
    
    func passDataToStudentsList(segue: UIStoryboardSegue)
    {
        // NOTE: Teach the router how to pass data to the next scene
        
//        print("called passDataToStudentsList")
//        let studentslViewController = segue.destination as! StudentsViewController
//        studentslViewController.output.selectedClass = UserManager.sharedInstance.getCurrentClass()

    }
}
