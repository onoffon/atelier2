//
//  ProfileViewController.swift
//  variations
//
//  Created by julien@macmini on 03/12/2016.
//  Copyright (c) 2016 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean varhitecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol ProfileViewControllerInput
{
    func displayClassList(viewModel: Profile.ClassList.ViewModel)
}

protocol ProfileViewControllerOutput
{
    func getClassList()
    func newClass(request: Profile.NewClass.Request)
    func setup()
    func tearDown()
    var selectedClass: Class! {get set}
    var classList: [Class]! {get}
    func selectClassFromCell(index: Int)
    func removeClass(index: Int)
}

class ProfileViewController: UIViewController, ProfileViewControllerInput, NewGroupDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, ProfilePresenterOutput
{
    var output: ProfileViewControllerOutput!
    var router: ProfileRouter!
    var classListToDisplay: [Profile.ClassList.ViewModel.displayedClass]!
    @IBOutlet weak var classCollectionView: UICollectionView!
    
    var showSuppressButtons: Bool = false {
        didSet{
            displaySuppressButtons(flag: showSuppressButtons)
        }
    }
    
    
    // MARK: - lifecycle
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func awakeFromNib()
    {
        super.awakeFromNib()
        ProfileConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        output.setup()
        getClassListOnLoad()
        
        // long press on collection view
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self,
                                                                               action: #selector(handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.3
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.classCollectionView?.addGestureRecognizer(lpgr)
        
        showSuppressButtons = false
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        output.tearDown()
        
    }
    
    
    // MARK: - CollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return classListToDisplay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "classCell", for: indexPath) as! ClassCollectionViewCell
        cell.classInfo = self.classListToDisplay[indexPath.row]
        cell.classSuppressButton.isHidden = !showSuppressButtons
        return cell
    }
    // MARK: - Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        // if user did a long press before, to remove classes:
        if (showSuppressButtons) {
            
            if UserManager.sharedInstance.selectedClass?.id == UserManager.sharedInstance.classList?[indexPath.row].id {
                let alert = UIAlertController(title: NSLocalizedString("Attention", comment: "titre popup erreur compte utilisateur"), message: NSLocalizedString("Vous ne pouvez pas supprimer une classe en cours d'utilisation. Veuillez d'abord choisir une autre classe ou choisir un compte invité.", comment: "popup erreur compte utilisateur"), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                    self.showSuppressButtons = false
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            let alert = UIAlertController(title: NSLocalizedString("Attention", comment: "titre popup erreur compte utilisateur"), message: NSLocalizedString("Vous êtes sur le point de supprimer une classe", comment: "popup avertissament suppression classe"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Annuler", comment: "suppression classe"), style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                self.showSuppressButtons = false
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Supprimer", comment: "confirmer suppresion classe"),
                                          style: UIAlertActionStyle.destructive,
                                          handler: {(alert: UIAlertAction!) in
                                            self.confirmedClassSuppress(index: indexPath.row)
                                            self.showSuppressButtons = false
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            output.selectClassFromCell(index: indexPath.row)
            performSegue(withIdentifier: "showStudentList", sender: self)
        }
        
    }
    func commitFormData(classInfo: Profile.NewClass.Request.ClassInfo){
        output.newClass(request: Profile.NewClass.Request(classInfo: classInfo))
    }
    // MARK: - Event handling
    
    func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        self.showSuppressButtons = true
    }
    
    func confirmedClassSuppress(index: Int){
        print("Remove class \(index)")
        output.removeClass(index: index)
    }
    
    @IBAction func newClassButtonTouched(_ sender: Any) {
        
        // present view controller with form, get school and class name
        
        let newClassForm: NewClassDialogViewController = self.storyboard?.instantiateViewController(withIdentifier: "newClassForm") as! NewClassDialogViewController
        newClassForm.modalPresentationStyle = .formSheet
        newClassForm.modalTransitionStyle = .coverVertical
        
        newClassForm.preferredContentSize = CGSize(width: self.view.bounds.size.width * 0.7, height:self.view.bounds.size.height * 0.7)
        self.present(newClassForm, animated: false, completion: {
            print("done presenting new class form")
        })
        
        newClassForm.delegate = self
    }
    
    @IBAction func selectGuestProfile(_ sendert: Any){
        
        UserManager.sharedInstance.loadGuestProfile()
        self.dismiss(animated: false, completion: nil)
    
    }
    
    
    func getClassListOnLoad(){
        
        output.getClassList()
        
    }
    
    @IBAction func doneTouched(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    // MARK: - routing
    
    // when another screen
    @IBAction func unwindToProfile(segue: UIStoryboardSegue) {
        output.setup()
    }
    
    // moved out of configurator, to allow unwind segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        router.passDataToNextScene(segue: segue)
    }
    
    
    // MARK: - Display logic
    
    func displaySuppressButtons(flag: Bool){
        for cell in classCollectionView.visibleCells{
            let classCell = cell as! ClassCollectionViewCell
            classCell.classSuppressButton.isHidden = !flag
        }
    }
    
    func displayClassList(viewModel: Profile.ClassList.ViewModel)
    {
        // NOTE: Display the result from the Presenter
        
        self.classListToDisplay = viewModel.displayedClassList
        
        classCollectionView.reloadData()
        
    }
}
