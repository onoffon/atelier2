//
//  ClassCollectionViewCell.swift
//  variations
//
//  Created by julien@macmini on 11/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

class ClassCollectionViewCell: UICollectionViewCell {
    
    
    //MARK: public API
    var classInfo:Profile.ClassList.ViewModel.displayedClass!{
        didSet{
            updateUI()
        }
    
    }
    
    //MARK: private 
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var classNameLabel: UILabel!
    @IBOutlet weak var classSuppressButton: UIButton!
    
    private func updateUI(){
        schoolNameLabel.text = classInfo.schoolName
        classNameLabel.text = classInfo.className
    
    }
    

    
}
