//
//  NewClassDialogViewController.swift
//  variations
//
//  Created by julien@macmini on 10/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation


protocol  NewGroupDelegate {
    func commitFormData(classInfo: Profile.NewClass.Request.ClassInfo)
}

class NewClassDialogViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var schoolName: UITextField!
    @IBOutlet weak var className: UITextField!
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconBackgroundImageView: UIImageView!
    var initConstantConstraint: CGFloat!
    var delegate: NewGroupDelegate?
    var hasMovedTextField: Bool = false
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        //square angles for view
        self.view.superview?.layer.cornerRadius = 0
        
        // underlying border only for text fields
        schoolName.borderAsGreyUnderline()
        className.borderAsGreyUnderline()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.superview?.layer.cornerRadius = 0
    }
    
    func keyboardWillShow(notification: NSNotification){
        let vc = self.presentingViewController
        
        // If this is an iPhone's interface
        if (vc?.traitCollection.verticalSizeClass == .compact){
            let value: NSValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardSize: CGRect  = value.cgRectValue;
            let maxYPosTextField: CGFloat = self.view.frame.origin.y + self.className.frame.origin.y + self.className.frame.size.height;
            
            if (self.view.frame.origin.y + self.view.frame.size.height - maxYPosTextField < keyboardSize.size.height){
                self.view.layoutIfNeeded()
                self.initConstantConstraint = self.verticalConstraint.constant
                self.verticalConstraint.constant -= keyboardSize.size.height - (self.view.frame.origin.y + self.view.frame.size.height - maxYPosTextField)

                self.hasMovedTextField = true
                UIView.animate(withDuration: 0.2, animations: {
                 self.iconBackgroundImageView.isHidden = true
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        if (self.hasMovedTextField){
            self.view.layoutIfNeeded()
            self.verticalConstraint.constant = self.initConstantConstraint
            UIView.animate(withDuration: 0.2, animations: {
                self.iconBackgroundImageView.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    @IBAction func validationButtonTouched(_ sender: Any) {
        
        if (schoolName.text != "" && className.text != ""){
            let classInfo = Profile.NewClass.Request.ClassInfo(schoolName: schoolName.text!, className: className.text!)
            
            delegate?.commitFormData(classInfo: classInfo)
            dismiss(animated: false, completion: nil)
        }
    }
    
    
    @IBAction func dismissButtonTouched(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - text field
    
    // when user hits return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // pass keyboard edition to next field
        if textField == schoolName {
            className.becomeFirstResponder()
        }
        // or hide keyboard
        if textField == className {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
}
