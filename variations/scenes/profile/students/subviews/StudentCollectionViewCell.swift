//
//  StudentCollectionViewCell.swift
//  variations
//
//  Created by julien@macmini on 11/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

class StudentCollectionViewCell: UICollectionViewCell {
    //MARK: public API
    var studentInfo:Students.List.ViewModel.displayedStudent!{
        didSet{
            updateUI()
        }
    }
    
    //MARK: private
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentSuppressButton: UIButton!
    
    private func updateUI(){
        studentNameLabel.text = studentInfo.name.uppercased()
    }
    
}
