//
//  NewStudentDialogViewController.swift
//  variations
//
//  Created by julien@macmini on 11/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation


protocol  NewStudentDelegate {
    func commitFormData(studentInfo: Students.New.Request.StudentInfo)
}

class NewStudentDialogViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var studentName: UITextField!
    
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconBackgroundImageView: UIImageView!
    var initConstantConstraint: CGFloat!
    var hasMovedTextField: Bool = false
    
    var delegate: NewStudentDelegate?
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        //square angles for view
        self.view.superview?.layer.cornerRadius = 0
        
        // underlying border only for text fields
        studentName.borderAsGreyUnderline()

    }
    
    func keyboardWillShow(notification: NSNotification){
        let vc = self.presentingViewController
        
        // If this is an iPhone's interface
        if (vc?.traitCollection.verticalSizeClass == .compact){
            let value: NSValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardSize: CGRect  = value.cgRectValue;
            let maxYPosTextField: CGFloat = self.view.frame.origin.y + self.studentName.frame.origin.y + self.studentName.frame.size.height;
            
            if (self.view.frame.origin.y + self.view.frame.size.height - maxYPosTextField < keyboardSize.size.height){
                self.view.layoutIfNeeded()
                self.initConstantConstraint = self.verticalConstraint.constant
                self.verticalConstraint.constant -= keyboardSize.size.height - (self.view.frame.origin.y + self.view.frame.size.height - maxYPosTextField)
                
                self.hasMovedTextField = true
                UIView.animate(withDuration: 0.2, animations: {
                    self.iconBackgroundImageView.isHidden = true
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        if (self.hasMovedTextField){
            self.view.layoutIfNeeded()
            self.verticalConstraint.constant = self.initConstantConstraint
            UIView.animate(withDuration: 0.2, animations: {
                self.iconBackgroundImageView.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.superview?.layer.cornerRadius = 0
    }
    
    
    @IBAction func validationButtonTouched(_ sender: Any) {
        
        if (studentName.text != "" ){

            let studentInfo = Students.New.Request.StudentInfo(name: studentName.text!)
            
            delegate?.commitFormData(studentInfo: studentInfo)
            dismiss(animated: false, completion: nil)
        }
    }
    
    
    @IBAction func dismissButtonTouched(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - text field
    
    // when user hits return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // hide keyboard
        if textField == studentName {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
}
