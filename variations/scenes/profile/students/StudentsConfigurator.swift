//
//  StudentsConfigurator.swift
//  variations
//
//  Created by julien@macmini on 11/01/2017.
//  Copyright (c) 2017 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

// MARK: - Connect View, Interactor, and Presenter

extension StudentsViewController: StudentsPresenterOutput
{
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    router.passDataToNextScene(segue: segue)
  }
}

extension StudentsInteractor: StudentsViewControllerOutput
{
}

extension StudentsPresenter: StudentsInteractorOutput
{
}

class StudentsConfigurator
{
  // MARK: - Object lifecycle
  
  static let sharedInstance = StudentsConfigurator()
  
  private init() {}
  
  // MARK: - Configuration
  
  func configure(viewController: StudentsViewController)
  {
    let router = StudentsRouter()
    router.viewController = viewController
    
    let presenter = StudentsPresenter()
    presenter.output = viewController
    
    let interactor = StudentsInteractor()
    interactor.output = presenter
    
    viewController.output = interactor
    viewController.router = router
  }
}
