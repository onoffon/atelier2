//
//  TutorialViewController.swift
//  variations
//
//  Created by julien@macmini on 14/09/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

protocol TutorialViewControllerDelegate {
    func tutorialPreviousPage()
    func tutorialNextPage()

}

class TutorialViewController: UIViewController {

    
    @IBOutlet weak var previousPageButton: UIButton!
    @IBOutlet weak var nextPageButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    var delegate: TutorialViewControllerDelegate! = nil
    
    var currentPage: Int = 0
    
    @IBAction func previousButtonTouched(_ sender: Any) {
        delegate.tutorialPreviousPage()
    }
    @IBAction func finishButtonTouched(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func nexButtonTouched(_ sender: Any) {
        delegate.tutorialNextPage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
