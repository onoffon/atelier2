//
//  MenuTableViewCell.swift
//  variations
//
//  Created by julien@macmini on 21/02/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var composerLabel: UILabel!
    @IBOutlet weak var arrowNext: UIButton!
    @IBOutlet weak var arrowPrevious: UIButton!
    
    var delegate: pieceMenuCellDelegate!
    var pieceIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBAction func loadPieceButton(_ sender: UIButton) {
        delegate.pushedLoadPieceButton(index: pieceIndex)
    }
    
    @IBAction func pushedPrevious(_ sender: UIButton) {
        delegate.pushedArrowPrevious(fromIndex: pieceIndex)
    }
    @IBAction func pushedNext(_ sender: UIButton) {
        delegate.pushedArrowNext(fromIndex: pieceIndex)
    }

    
}
