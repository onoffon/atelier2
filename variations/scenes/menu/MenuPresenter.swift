//
//  MenuPresenter.swift
//  variations
//
//  Created by julien@macmini on 02/12/2016.
//  Copyright (c) 2016 jbloit. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol MenuPresenterInput
{
    func presentCatalog(response: Menu.FetchCatalog.Response)
    func presetStudent(response: Menu.GetStudent.Response)
    func showProgress()
    func dismissProgress()
}

protocol MenuPresenterOutput: class
{
    func displayCatalog(viewModel: Menu.FetchCatalog.ViewModel)
    func displayStudent(viewModel: Menu.GetStudent.ViewModel)
    func showProgress()
    func dismissProgress()
}

class MenuPresenter: MenuPresenterInput
{
    weak var output: MenuPresenterOutput!
    
    
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    
    
    // MARK: - Presentation logic
    
    func presentCatalog(response: Menu.FetchCatalog.Response)
    {
        // NOTE: Format the response from the Interactor and pass the result back to the View Controller
        
        let documentsDirectory = paths[0]
        let imgageFolderPath = documentsDirectory.appendingPathComponent("image", isDirectory: true)
        
        
        var displayedCatalog : [Menu.FetchCatalog.ViewModel.displayedPiece] = []
        for piece in response.catalog {
//            let stubImageURL: URL = imgageFolderPath.appendingPathComponent("composerImage.png", isDirectory: false)
            let imageURL = imgageFolderPath.appendingPathComponent(piece.imageLocalReference!, isDirectory: false)
            let displayedPiece = Menu.FetchCatalog.ViewModel.displayedPiece(title: piece.title!.uppercased(),
                                                                            subtitle: piece.subtitle!.uppercased(),
                                                                            composer: piece.composer!.uppercased(),
                                                                            imageURL: imageURL)
            displayedCatalog.append(displayedPiece)
        }
        let viewModel = Menu.FetchCatalog.ViewModel(displayedCatalog: displayedCatalog)
        self.output.displayCatalog(viewModel: viewModel)
    }
    

    func presetStudent(response: Menu.GetStudent.Response){
        
        output.displayStudent(viewModel: Menu.GetStudent.ViewModel(name: (response.student.name?.uppercased())!))
        
    }
    func showProgress(){
        output.showProgress()
    }
    func dismissProgress(){
        output.dismissProgress()
    }
    
}
