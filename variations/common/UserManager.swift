//
//  UserManager.swift
//  variations
//
//  Created by julien@macmini on 06/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation
import Firebase

@objc
enum UserDataStatus : Int {
    case empty
    case updating
    case updated
}

class UserManager: NSObject {
    
    /// Singleton
    static let sharedInstance = UserManager()
    
    /// Top level node for user data:
    let ref = Database.database().reference(withPath: "classes")
    
    /// Current logged user id
    var uid : String?
    
    /// Data updating status
    dynamic var status: UserDataStatus = .empty
    
    // Selected class node
    var selectedClassRef: DatabaseReference?
    var selectedClassId: String?
    var selectedClass: Class?
    var selectedStudentIndexInClass: Int! = -1 // -1 codes for guest
    var selectedStudent: Student! {
        didSet{
            // for observers to be notified
            status = .updating
            status = .updated
        }
    }
    
    /// a guest profile student
    private var guestStudent: Student?
    
    /// store all of a user's current settings in this preset
    var currentPreset: PiecePreset!
    
    var currentIsGuest: Bool = true
    
    /**
     Current selected student path.
     Optional: in case no student is selected, this value is nil and a guest user's preset is used.
     */
    var currentStudentPath: DatabaseReference?
    
    /// Current selected preset path
    var currentPresetPath: DatabaseReference?
    
    /// list of user data for current logged user
    var classList: [Class]!
    
    // MARK: - life cycle
    func setup(){
        DataManager.sharedInstance.addObserver(self, forKeyPath: "catalogStatus", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let newValue = change?[NSKeyValueChangeKey.newKey] {
            
            // on status change in DataManager:
            if (keyPath! == "catalogStatus"){
                print("Status changed: \(newValue)")
                let status: Int = newValue as! Int
                if (status == CatalogStatus.filledCatalog.rawValue){
                    if (guestStudent == nil){
                        self.initGuest()
                    }
                    
                }
            }
        }
    }
    
    /**
     Create, init the guestStudent:Student property, and set it as the selected student.
     */
    func initGuest(){
        status = .updating
        guestStudent = Student(name: NSLocalizedString("Invité", comment: "default student label on menu page"))

        /// array of piece presets, one per piece in the catalog.
        var guestPresets: [PiecePreset] = []
        for piece in DataManager.sharedInstance.catalog {
            let preset = PiecePreset(forPiece: piece)
            guestPresets.append(preset)
        }
        guestStudent?.presets = guestPresets
        loadGuestProfile()
        status = .updated
    }
    
    
    // MARK: - API
    
    /// ID of the current FireBase logged-in user
    func getCurrentUserID()->String?{
        let user = Auth.auth().currentUser
        return user?.uid
    }
    
    /// add a new student and set as current student
    func addStudent(student: Student, toClassWithId: String) -> String{
        
        var studentToAdd = student
        // Add piece presets for new student
        var studentPresets: [PiecePreset] = []
        for piece in DataManager.sharedInstance.catalog {
            
            let preset = PiecePreset(forPiece : piece)
            studentPresets.append(preset)
        }
        studentToAdd.presets = studentPresets
        
        
        // Add student to DB
        let newStudentNodePath = ref.child(toClassWithId).child("students").childByAutoId()
        newStudentNodePath.setValue(studentToAdd.toAnyObject())
        self.currentStudentPath = newStudentNodePath
        return newStudentNodePath.key
    }
    
    // Remove a student.
    // Can't remove the one that is currently selected. Have to select guest before that.
    // Display a message in this case. 
    
    func removeStudent(index: Int, completion: @escaping () -> Void){

        if selectedClass != nil {
            let student: Student? = selectedClass?.students?[index]
            if student != nil {
                let removeRef = ref.child(self.selectedClassId!).child("students").child((student?.id)!)

                removeRef.removeValue(completionBlock: { (error, refer) in
                    // should just trigger a refresh of the collection view
                    completion()
                })
            } else {
                print("couldn't find student to remove")
            }
        }
    }
    
    
    // Remove a class.
    // Can't remove the one that is currently selected. Have to select guest before that.
    // Display a message in this case.
    
    func removeClass(index: Int, completion: @escaping () -> Void){
        
        
        if classList != nil {
            let classToRemove: Class? = classList?[index]
            if classToRemove != nil {
                let removeRef = ref.child((classToRemove?.id)!)
                
                removeRef.removeValue(completionBlock: { (error, refer) in
                    // should just trigger a refresh of the collection view
                    //completion()
                })
            } else {
                print("couldn't find class to remove")
            }
        }
    }
    
    func commitClass(newClass: Class){
        let newClassNodePath = ref.childByAutoId()
        newClassNodePath.setValue(newClass.toAnyObject())
        
    }

    // return the selected class according to a given id
    func selectClass(withId: String){
        self.selectedClassId = withId
        selectedClassRef = ref.child(selectedClassId!)
    }
    
    func selectStudent(with index: Int){
        if selectedClass != nil {
            
            let student: Student? = selectedClass?.students?[index]
            if student != nil {
                selectedStudent = student
                selectedStudentIndexInClass = index
                currentIsGuest = false
            } else {
                loadGuestProfile()
                currentIsGuest = true
            }
        }
    }

    func getCurrentClass(completion: @escaping (_ result: Class) -> Void){
        ref.child(selectedClassId!).observeSingleEvent(of: .value, with: { (snapshot) in
            self.selectedClass = Class(snapshot: snapshot)
            print("[User Manager] updated selected class ")
            completion(self.selectedClass!)
        })
    }
    
    func getClassList()->[Class]{
        return self.classList
    }
    
    /// Update class list with observer
    func observeClassList(){
        
        ref.queryOrdered(byChild: "ownerId").queryEqual(toValue: uid).observe(.value, with: { snapshot in
            self.classList = []
            self.status = UserDataStatus.updating
            for item in snapshot.children {
                let classDict = (item as! DataSnapshot).value as? [String: AnyObject]
                //                let newClass = Class(id:(item as! FIRDataSnapshot).key, ownerId: self.uid!, dict: classDict!)
                let newClass = Class(snapshot: item as! DataSnapshot)
                self.classList.append(newClass)
            }
            self.status = UserDataStatus.updated
        })
    }
    
    /// Set the selected student to guest and init "selected*" proerties accordingly.
    func loadGuestProfile(){
        
        selectedClassRef = nil
        selectedClass = nil
        selectedClassId = nil
        selectedStudentIndexInClass = -1
        
        selectedStudent = guestStudent

        currentIsGuest = true
    }
    
    /**
     Get the piece preset for the current user, for a given selected piece. If no preset
     is found, create a new one.
    */
    func getPresetForPiece(with piece:Piece, completion: @escaping (_ result: PiecePreset) -> Void){

        // If user selected a named profile
        if (!currentIsGuest){
            
            selectedClassRef?.child("students").child(selectedStudent.id).observeSingleEvent(of: .value, with: { (snapshot: DataSnapshot) in
                
                // If student has presets
                if snapshot.hasChild("presets") {
                    
                    print("student has presets")

                    // see if preset exists for given piece
                    var foundPiece:Bool = false
                    let presetsSnapshot = snapshot.childSnapshot(forPath: "presets")
                    
                    for item in presetsSnapshot.children {
                        let snapshotItem = item as! DataSnapshot
                        if snapshotItem.key == piece.key {
                            foundPiece = true
                            self.currentPreset = PiecePreset(snapshot: snapshotItem)
                            self.currentPresetPath = snapshotItem.ref
                            completion(self.currentPreset)
                        }
                    }
                    
                    // add piece preset if not present already
                    if (!foundPiece){
                        print("couldn't find piece preset")
                        // add preset node
                        
                        self.currentPreset = PiecePreset(pieceId: piece.key,
                                                         numberOfBarsInLoop: piece.numberOfBarsInLoop!,
                                                         numberOfLoopsInSolo: (piece.solo?.numberOfLoops)!,
                                                         numberOfLoopsInContinuo: (piece.continuo?[0].numberOfLoops)!)
                        
                        snapshot.ref.child("presets").child(piece.key).setValue(self.currentPreset.toAnyObject())
                        self.currentPresetPath = snapshot.ref.child("presets").child(piece.key)
                        completion(self.currentPreset)
                    }
                    
                }
                else {
                    print("student has no presets")
                    
                }
            })
        }
            // Using guest profile
        else {
            for preset in selectedStudent.presets{
                if (preset.pieceId == piece.key){
                    currentPreset = preset
                    completion(preset)
                    break
                }
            }
        }
    }
    
    // Save sequence to current preset in DB tree
    func commitToCurrentPreset(sequence: [Int], playMode: Instrument, slotIndex: Int, continuoPartIndex: Int? = 0 ){
        
        print("commit current preset, unless if current user is guest")
        
        if (!currentIsGuest){
            switch playMode {
            case Instrument.solo:
                print("commit sequence for solo")
                currentPreset.solo.sequences[slotIndex] = sequence
                currentPresetPath?.child("solo/sequences").child(String(slotIndex)).setValue(sequence)

            case Instrument.all:
                print("commit sequence for all playmode")
                currentPreset.solo.sequences[slotIndex] = sequence
                currentPresetPath?.child("solo/sequences").child(String(slotIndex)).setValue(sequence)
                
            case Instrument.continuo:
                print("commit sequence for continuo")
                if let partIndex = continuoPartIndex {
                    currentPreset.continuo[partIndex].sequences[slotIndex] = sequence
                    currentPresetPath?.child("continuo/\(partIndex)/sequences").child(String(slotIndex)).setValue(sequence)
                }
            }
        } else {
            switch playMode {
            case Instrument.solo:
                print("guest-commit sequence for solo")
                currentPreset.solo.sequences[slotIndex] = sequence
            case Instrument.all:
                print("guest-commit sequence for all playmode")
                currentPreset.solo.sequences[slotIndex] = sequence
            case Instrument.continuo:
                print("guest-commit sequence for continuo")
                if let partIndex = continuoPartIndex {
                    currentPreset.continuo[partIndex].sequences[slotIndex] = sequence
                }
            }
        }
    }
    
    // Save pattern to current piece preset.
    func commitToCurrentPreset(pattern: [[Int]], playMode: Instrument, variationIndex: Int, continuoPartIndex: Int? = 0){
        if (!currentIsGuest){
            switch playMode {
            case Instrument.solo:
                print("commit pattern for solo")
                currentPreset.solo.variationPatterns[variationIndex] = pattern
                currentPresetPath?.child("solo/variationPatterns").child(String(variationIndex)).setValue(pattern)
                
            case Instrument.all:
                print("commit pattern for all playmode")
                currentPreset.solo.variationPatterns[variationIndex] = pattern
                currentPresetPath?.child("solo/variationPatterns").child(String(variationIndex)).setValue(pattern)
                
            case Instrument.continuo:
                print("commit pattern for continuo")
                if let partIndex = continuoPartIndex{
                    currentPreset.continuo[partIndex].variationPatterns[variationIndex] = pattern
                    currentPresetPath?.child("continuo/\(partIndex)/variationPatterns").child(String(variationIndex)).setValue(pattern)
                }
            }
        } else {
            switch playMode {
            case Instrument.solo:
                print("guest-commit pattern for solo")
                currentPreset.solo.variationPatterns[variationIndex] = pattern
                
            case Instrument.all:
                print("guest-commit pattern for all playmode")
                currentPreset.solo.variationPatterns[variationIndex] = pattern
            case Instrument.continuo:
                print("guest-commit pattern for continuo")
                if let partIndex = continuoPartIndex{
                    currentPreset.continuo[partIndex].variationPatterns[variationIndex] = pattern
                }
            }
        }
    }
    
    // Save variation name to current piece preset.
    func commitToCurrentPreset(variationName: String, playMode: Instrument, variationIndex: Int, continuoPartIndex: Int? = 0){
            switch playMode {
            case Instrument.solo:
                print("commit variation name for solo")
                currentPreset.solo.variationNames[variationIndex] = variationName
                currentPresetPath?.child("solo/variationNames").child(String(variationIndex)).setValue(variationName)
                
            case Instrument.all:
                print("commit variation name for all playmode")
                currentPreset.solo.variationNames[variationIndex] = variationName
                currentPresetPath?.child("solo/variationNames").child(String(variationIndex)).setValue(variationName)
                
            case Instrument.continuo:
                print("commit variation name for continuo")
                if let partIndex = continuoPartIndex{
                    currentPreset.continuo[partIndex].variationNames[variationIndex] = variationName
                    currentPresetPath?.child("continuo/\(partIndex)/variationNames").child(String(variationIndex)).setValue(variationName)
                }
        }
    }
    
    func anonymousLogin(){
        
        // Anonymous login
        Auth.auth().signInAnonymously() { (user, error) in
            if (error == nil){
                self.uid = user!.uid
                print("signed in with user id \(self.uid)")
                self.observeClassList()
                
            } else {
                print("Sign in error \(error?.localizedDescription)")
                
            }
        }
        
    }
    
}
