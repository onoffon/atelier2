//
//  UILabelTitle.swift
//  variations
//
//  Created by julien@macmini on 22/02/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import UIKit

class UILabelTitle: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var border = CALayer()
    let width = CGFloat(2.0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }

    func customInit(){
        
        border.borderColor = UIColor(hexString: lightGrey).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    func setUnderlinerColor(color: UIColor){
        border.borderColor = color.cgColor
    }
    
}
