//
//  AudioManager.m
//  variations
//
//  Created by julien@macmini on 10/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

#import "AudioManager.h"
#import <Foundation/Foundation.h>
#include <AVFoundation/AVAudioSession.h>
#include <libkern/OSAtomic.h>
#include <mach/mach_time.h>
#import "CAXException.h"
#import "CAComponentDescription.h"

#define INVALID_BEAT_TIME DBL_MIN
#define INVALID_BPM DBL_MIN

const Float64 kGraphSampleRate = 44100.0;
static OSSpinLock lock;
static AudioManager *_sharedInstance = nil;

@interface AudioManager (){
    
    AUGraph _graph;
    AudioUnit _fileAU_solo_odd;
    AudioUnit _fileAU_solo_even;
    AudioUnit _outputAU;
    AudioUnit _mixerAU;
    AudioStreamBasicDescription _asbd;
    LinkData _linkData;
}

@end

@implementation AudioManager

#pragma mark- life cycle

- (void) initVars{
    isLoading = false;
    [self initLinkData:60];
    // allocate the mSoundBuffer struct
    memset(&mSoundBuffer, 0, sizeof(mSoundBuffer));
    
    // allocate sinewave data struct
    memset(&mSinewaveData, 0, sizeof(mSinewaveData));
    _isGraphPlaying = NO;
    self.loadStatus = empty;
    urlCount = 0;
    
}

- (void) setupSession{
    try {
        NSError *error = nil;
        
        // Configure the audio session
        AVAudioSession *sessionInstance = [AVAudioSession sharedInstance];
        
        // our default category -- we change this for conversion and playback appropriately
        [sessionInstance setCategory:AVAudioSessionCategoryPlayback error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set audio category");
        
        NSTimeInterval bufferDuration = .005;
        [sessionInstance setPreferredIOBufferDuration:bufferDuration error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set IOBufferDuration");
        
        double hwSampleRate = 44100.0;
        [sessionInstance setPreferredSampleRate:hwSampleRate error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set preferred sample rate");
        
        // add interruption handler
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleInterruption:)
                                                     name:AVAudioSessionInterruptionNotification
                                                   object:sessionInstance];
        
        // we don't do anything special in the route change notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleRouteChange:)
                                                     name:AVAudioSessionRouteChangeNotification
                                                   object:sessionInstance];
        
        // activate the audio session
        [sessionInstance setActive:YES error:&error];
        XThrowIfError((OSStatus)error.code, "couldn't set audio session active\n");
        
        // just print out the sample rate
        printf("Hardware Sample Rate: %.1f Hz\n", sessionInstance.sampleRate);
    } catch (CAXException e) {
        char buf[256];
        fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
        printf("You probably want to fix this before continuing!");
    }
}

- (void)handleInterruption:(NSNotification *)notification
{
    UInt8 theInterruptionType = [[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
    
    NSLog(@"Session interrupted > --- %s ---\n", theInterruptionType == AVAudioSessionInterruptionTypeBegan ? "Begin Interruption" : "End Interruption");
	   
    if (theInterruptionType == AVAudioSessionInterruptionTypeBegan) {
    }
    
    if (theInterruptionType == AVAudioSessionInterruptionTypeEnded) {
        // make sure to activate the session
        NSError *error = nil;
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        
        if (nil != error) NSLog(@"AVAudioSession set active failed with error: %@", error);
    }
}


- (void)handleRouteChange:(NSNotification *)notification
{
    UInt8 reasonValue = [[notification.userInfo valueForKey:AVAudioSessionRouteChangeReasonKey] intValue];
    AVAudioSessionRouteDescription *routeDescription = [notification.userInfo valueForKey:AVAudioSessionRouteChangePreviousRouteKey];
    
    NSLog(@"Route change:");
    switch (reasonValue) {
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            NSLog(@"     NewDeviceAvailable");
            break;
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            NSLog(@"     OldDeviceUnavailable");
            break;
        case AVAudioSessionRouteChangeReasonCategoryChange:
            NSLog(@"     CategoryChange");
            NSLog(@" New Category: %@", [[AVAudioSession sharedInstance] category]);
            break;
        case AVAudioSessionRouteChangeReasonOverride:
            NSLog(@"     Override");
            break;
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            NSLog(@"     WakeFromSleep");
            break;
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            NSLog(@"     NoSuitableRouteForCategory");
            break;
        default:
            NSLog(@"     ReasonUnknown");
    }
    
    NSLog(@"Previous route:\n");
    NSLog(@"%@", routeDescription);
}


- (void)dealloc {
    
    ABLLinkDelete(_linkData.ablLink);
}


#pragma mark- Link support


- (void)initLinkData:(Float64)bpm {
    mach_timebase_info_data_t timeInfo;
    mach_timebase_info(&timeInfo);
    
    lock = OS_SPINLOCK_INIT;
    _linkData.ablLink = ABLLinkNew(bpm);
    _linkData.sampleRate = kGraphSampleRate;
    _linkData.secondsToHostTime = (1.0e9 * timeInfo.denom) / (Float64)timeInfo.numer;
    _linkData.sharedEngineData.outputLatency =
    (UInt32)(_linkData.secondsToHostTime * [AVAudioSession sharedInstance].outputLatency);
    _linkData.sharedEngineData.resetToBeatTime = INVALID_BEAT_TIME;
    _linkData.sharedEngineData.proposeBpm = INVALID_BPM;
    _linkData.sharedEngineData.quantum = 4; // quantize to 4 beats
    _linkData.sharedEngineData.isPlaying = NO;
    for (int i = 0; i < NUMFILES; i++){
        _linkData.sharedEngineData.soloPart[i] = 1;
    }
    _linkData.timeAtLastClick = 0;
    _linkData.timeAtLastBar = 0;
    _linkData.barPhaseAtLastClick = 0;
    _linkData.barIndex = 0;
    // init variation sequence
    _linkData.sequenceSize = 8;
    
    //
    for (int i = 0; i<_linkData.sequenceSize; i++){
        _linkData.sharedEngineData.sequence[0][i] = i % 3;
    }
    
    _linkData.currentTempo = INVALID_BPM;
    
    _linkData.sharedEngineData.instrument = all;
    
    _linkData.sharedEngineData.playStatus = stopped;
    
    _linkData.localEngineData = _linkData.sharedEngineData;
}

/*
 * Pull data from the main thread to the audio thread if lock can be
 * obtained. Otherwise, just use the local copy of the data.
 */
static void pullEngineData(LinkData* linkData, EngineData* output) {
    // Always reset the signaling members to their default state
    output->resetToBeatTime = INVALID_BEAT_TIME;
    output->proposeBpm = INVALID_BPM;
    
    // Attempt to grab the lock guarding the shared engine data but
    // don't block if we can't get it.
    if (OSSpinLockTry(&lock)) {
        // Copy non-signaling members to the local thread cache
        linkData->localEngineData.outputLatency = linkData->sharedEngineData.outputLatency;
        linkData->localEngineData.quantum = linkData->sharedEngineData.quantum;
        linkData->localEngineData.isPlaying = linkData->sharedEngineData.isPlaying;
        
        for (int partIndex = 0; partIndex<MAXCONTINUOPARTS; partIndex++){
            for (int i=0; i<linkData->sequenceSize; i++){
                linkData->localEngineData.sequence[partIndex][i] = linkData->sharedEngineData.sequence[partIndex][i];
            }
        }
        
        // Copy signaling members directly to the output and reset
        output->resetToBeatTime = linkData->sharedEngineData.resetToBeatTime;
        linkData->sharedEngineData.resetToBeatTime = INVALID_BEAT_TIME;
        
        output->proposeBpm = linkData->sharedEngineData.proposeBpm;
        linkData->sharedEngineData.proposeBpm = INVALID_BPM;
        
        linkData->localEngineData.instrument = linkData->sharedEngineData.instrument;
        
        linkData->localEngineData.playStatus = linkData->sharedEngineData.playStatus;
        
        for (int i = 0; i < NUMFILES; i++){
            linkData->localEngineData.soloPart[i] = linkData->sharedEngineData.soloPart[i];
        }
        OSSpinLockUnlock(&lock);
    }
    
    // Copy from the thread local copy to the output. This happens
    // whether or not we were able to grab the lock.
    output->outputLatency = linkData->localEngineData.outputLatency;
    output->quantum = linkData->localEngineData.quantum;
    output->isPlaying = linkData->localEngineData.isPlaying;
}

#pragma mark- render proc and graph setup

/*
 * The metronome audio callback. Query or reset the beat time and generate audible clicks
 * corresponding to beat time of the current buffer.
 */
static OSStatus linkMetronomeProc(
                                  void *inRefCon,
                                  AudioUnitRenderActionFlags *flags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    
    // First clear buffers
    for (UInt32 i = 0; i < ioData->mNumberBuffers; ++i) {
        memset(ioData->mBuffers[i].mData, 0, inNumberFrames * sizeof(Float32));
    }
    
    
    LinkData *linkData = (LinkData *)inRefCon;
    
    // Get a copy of the current link timeline.
    const ABLLinkTimelineRef timeline =
    ABLLinkCaptureAudioTimeline(linkData->ablLink);
    
    // Get a copy of relevant engine parameters.
    EngineData engineData;
    pullEngineData(linkData, &engineData);
    
    // The mHostTime member of the timestamp represents the time at
    // which the buffer is delivered to the audio hardware. The output
    // latency is the time from when the buffer is delivered to the
    // audio hardware to when the beginning of the buffer starts
    // reaching the output. We add those values to get the host time
    // at which the first sample of this buffer will reach the output.
    const UInt64 hostTimeAtBufferBegin =
    inTimeStamp->mHostTime + engineData.outputLatency;
    
    // Handle a timeline reset
    if (engineData.resetToBeatTime != INVALID_BEAT_TIME) {
        // Reset the beat timeline so that the requested beat time
        // occurs near the beginning of this buffer. The requested beat
        // time may not occur exactly at the beginning of this buffer
        // due to quantization, but it is guaranteed to occur within a
        // quantum after the beginning of this buffer. The returned beat
        // time is the actual beat time mapped to the beginning of this
        // buffer, which therefore may be less than the requested beat
        // time by up to a quantum.
        ABLLinkRequestBeatAtTime(
                                 timeline, engineData.resetToBeatTime, hostTimeAtBufferBegin,
                                 engineData.quantum);
    }
    
    // Handle a tempo proposal
    if (engineData.proposeBpm != INVALID_BPM) {
        // Propose that the new tempo takes effect at the beginning of
        // this buffer.
        ABLLinkSetTempo(timeline, engineData.proposeBpm, hostTimeAtBufferBegin);
    }
    
    // When playing, render the metronome sound
    if (engineData.isPlaying) {
        // Only render the metronome sound to the first channel. This
        // might help with source separate for timing analysis.
        renderMetronomeIntoBuffer(
                                  timeline, engineData.quantum, hostTimeAtBufferBegin, linkData->sampleRate,
                                  linkData->secondsToHostTime, inNumberFrames, &linkData->timeAtLastClick,
                                  &linkData->timeAtLastBar, &linkData->barPhaseAtLastClick, &linkData->barIndex,
                                  linkData->sequenceSize, (Float32*)ioData->mBuffers[0].mData);
    }
    
    linkData->currentTempo = ABLLinkGetTempo(timeline);
    
    ABLLinkCommitAudioTimeline(linkData->ablLink, timeline);
    
    return noErr;
}


/*
 * Render a metronome sound into the given buffer according to the
 * given timeline and quantum.
 */
static void renderMetronomeIntoBuffer(
                                      const ABLLinkTimelineRef timeline,
                                      const Float64 quantum,
                                      const UInt64 beginHostTime,
                                      const Float64 sampleRate,
                                      const Float64 secondsToHostTime,
                                      const UInt32 bufferSize,
                                      UInt64* timeAtLastClick,
                                      UInt64* timeAtLastBar,
                                      UInt64* barPhaseAtLastClick,
                                      UInt8* barIndex,
                                      UInt8 sequenceSize,
                                      Float32* buffer)
{
    // Metronome frequencies
    static const Float64 highTone = 1567.98;
    static const Float64 lowTone = 1108.73;
    // 100ms click duration
    static const Float64 clickDuration = 0.1;
    
    // The number of host ticks that elapse between samples
    const Float64 hostTicksPerSample = secondsToHostTime / sampleRate;
    
    for (UInt32 i = 0; i < bufferSize; ++i) {
        Float32 amplitude = 0.;
        // Compute the host time for this sample.
        const UInt64 hostTime = beginHostTime + llround(i * hostTicksPerSample);
        const UInt64 lastSampleHostTime = hostTime - llround(hostTicksPerSample);
        
        
        // Only make sound for positive beat magnitudes. Negative beat
        // magnitudes are count-in beats.
        if (ABLLinkBeatAtTime(timeline, hostTime, quantum) >= 0.) {
            
            // If the phase wraps around between the last sample and the
            // current one with respect to a 1 beat quantum, then a click
            // should occur.
            if (ABLLinkPhaseAtTime(timeline, hostTime, 1) <
                ABLLinkPhaseAtTime(timeline, lastSampleHostTime, 1)) {
                *timeAtLastClick = hostTime;
                
                // only query bar phase on beats
                const double barPhase = ABLLinkPhaseAtTime(timeline, hostTime, quantum);
                //                printf("barPhase %f\n", barPhase);
                
                if (barPhase < *barPhaseAtLastClick){
                    
                    *timeAtLastBar = hostTime;
                    *barIndex = (*barIndex + 1) % sequenceSize;
                }
                *barPhaseAtLastClick = barPhase;
            }
            
            const Float64 secondsAfterClick =
            (hostTime - *timeAtLastClick) / secondsToHostTime;
            
            // If we're within the click duration of the last beat, render
            // the click tone into this sample
            if (secondsAfterClick < clickDuration) {
                // If the phase of the last beat with respect to the current
                // quantum was zero, then it was at a quantum boundary and we
                // want to use the high tone. For other beats within the
                // quantum, use the low tone.
                
                
                
                Float64 freq = lowTone;
                if ( floor(ABLLinkPhaseAtTime(timeline, hostTime, quantum)) == 0){
                    freq = highTone;
                }
                // Simple cosine synth
                amplitude =
                cos(2 * M_PI * secondsAfterClick * freq) *
                (1 - sin(5 * M_PI * secondsAfterClick));
            }
        }
        //        buffer[i] = amplitude;
        buffer[i] = 0;
    }
}


// audio render procedure, don't allocate memory, don't take any locks, don't waste time
static OSStatus renderInput(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
    
    // bus numbers for the audio players are indexed from one in the graph setup. buffer indices start at 0, so:
    UInt16 audioPlayerChannel = inBusNumber - 1;
    
    // get user data
    LinkData* linkData = (LinkData*)inRefCon;
    SoundBufferPtr sndbuf = linkData->audioBuffersPtr;
    
    // Get a copy of relevant engine parameters.
    EngineData engineData;
    pullEngineData(linkData, &engineData);
    const UInt64 hostTimeAtBufferBegin = inTimeStamp->mHostTime + engineData.outputLatency;
    // The number of host ticks that elapse between samples
    const Float64 hostTicksPerSample = linkData->secondsToHostTime / linkData->sampleRate;
    
    UInt32 sample = sndbuf[audioPlayerChannel].sampleNum;      // frame number to start from
    UInt32 bufSamples = sndbuf[audioPlayerChannel].numFrames;  // total number of frames in the sound buffer
    const double beatDuration = 60.0 / linkData->currentTempo;
    
    // total number of frames in the a bar
    UInt32 samplesInBar = linkData->localEngineData.quantum * beatDuration * linkData->sampleRate;
    // total number of frames in the loop sequence
    UInt32 samplesInLoop = linkData->sequenceSize * samplesInBar;
    
    
    Float32 *in = sndbuf[audioPlayerChannel].data; // audio data buffer
    
    Float32 *outA = (Float32 *)ioData->mBuffers[0].mData; // output audio buffer for L channel
    Float32 *outB = (Float32 *)ioData->mBuffers[1].mData; // output audio buffer for R channel
    
    UInt32 playHead;
    UInt32 variationOffset;
    for (UInt32 i = 0; i < inNumberFrames; ++i) {
        
        const UInt64 hostTime = hostTimeAtBufferBegin + llround(i * hostTicksPerSample);
        UInt8 barIndex = linkData->barIndex;
        
        // A bar is playing either notes from a variation, or the sustain sound from the previous bar.
        // Adjust the bar index to play either notes or sustain :
        
        // The even audio channel indices play the "odd_bar" files and vice versa:
        if ((linkData->barIndex + audioPlayerChannel) % 2 != 0){
            // this is a sustain bar
            barIndex -= 1;
            if (barIndex < 0){
                barIndex = linkData->sequenceSize - 1;
            }
        }
        
        
//        // If new beat
//        if (hostTime == linkData->timeAtLastClick) {
//            //            printf("---- BEAT\n");
//        }
        
        // if new bar
        
        if (hostTime == linkData->timeAtLastBar) {
            sample = linkData->barIndex * samplesInBar;
            
            if (linkData->localEngineData.playStatus == armed){
                linkData->sharedEngineData.playStatus = playing;
            }
            //            printf("* BAR %i, variation index %i at tempo %f\n", linkData->barIndex, linkData->localEngineData.sequence[linkData->barIndex], linkData->currentTempo);
        }
        
        sample++;
        
        switch (linkData->localEngineData.playStatus){
            case armed:
                outA[i] = outB[i] = 0;
                break;
                
            case stopped:
                outA[i] = outB[i] = 0;
                break;
                
            case playing:
                
                switch (linkData->localEngineData.instrument) {
                    case solo:
                        
                        variationOffset = linkData->localEngineData.sequence[0][barIndex] * samplesInLoop;
                        
                        if (audioPlayerChannel < 2){
                            playHead = sample + variationOffset;
                            if (playHead < bufSamples){ // check playhead is within buffer
                                outA[i] = outB[i] = in[playHead];
                            } else {
                                outA[i] = outB[i] = 0;
                            }
                            
                        } else {
                            outA[i] = outB[i] = 0;
                        }
                        
                        break;
                    case continuo:
                        variationOffset = linkData->localEngineData.sequence[0][barIndex] * samplesInLoop;

                        //
                        if (audioPlayerChannel > 1){
                            playHead = sample;
                            double volume = 1;
                            
                            if (audioPlayerChannel == 2 || audioPlayerChannel == 3){
                                variationOffset = linkData->localEngineData.sequence[0][barIndex] * samplesInLoop;
                                playHead = sample + variationOffset;
                                volume = 0.5 * linkData->localEngineData.soloPart[audioPlayerChannel];
                            }
                            
                            if (audioPlayerChannel == 4 || audioPlayerChannel == 5){
                                variationOffset = linkData->localEngineData.sequence[1][barIndex] * samplesInLoop;
                                playHead = sample + variationOffset;
                                volume = 0.5 * linkData->localEngineData.soloPart[audioPlayerChannel];
                            }
                            
                            if (audioPlayerChannel == 6 || audioPlayerChannel == 7){
                                variationOffset = linkData->localEngineData.sequence[2][barIndex] * samplesInLoop;
                                playHead = sample + variationOffset;
                                volume = 0.5 * linkData->localEngineData.soloPart[audioPlayerChannel];
                            }
                            
                            if (audioPlayerChannel == 8 || audioPlayerChannel == 9){
                                variationOffset = linkData->localEngineData.sequence[3][barIndex] * samplesInLoop;
                                playHead = sample + variationOffset;
                                volume = 0.5 * linkData->localEngineData.soloPart[audioPlayerChannel];
                            }
                            
                            if (playHead < bufSamples){ // check playhead is within buffer
                                outA[i] = outB[i] = in[playHead] * volume;
                            } else {
                                outA[i] = outB[i] = 0;
                            }
                            
                        } else {
                            
                            // Mute solo part
                            outA[i] = outB[i] = 0;
                        }
                        
                        break;
                    case all:
                        variationOffset = linkData->localEngineData.sequence[0][barIndex] * samplesInLoop;
                        
                        if (audioPlayerChannel > 1){
                            playHead = sample;
                        } else {
                            playHead = sample + variationOffset;
                        }
                        if (playHead < bufSamples){ // check playhead is within buffer
                            outA[i] = outB[i] = in[playHead];
                        } else {
                            outA[i] = outB[i] = 0;
                        }
                        
                        break;
                }
                
                break;
        }

        if ((sample > bufSamples) ||  (sample > samplesInLoop)){
            // start over from the beginning of the data, our audio simply loops
            //            printf("looping data for bus %d after %ld source frames rendered\n", (unsigned int)audioPlayerChannel, (long)sample-1);
            sample = 0;
        }
    }
    
    sndbuf[audioPlayerChannel].sampleNum = sample; // keep track of where we are in the source data buffer
    
    //printf("bus %d sample %d\n", (unsigned int)inBusNumber, (unsigned int)sample);
    
    return noErr;
}

// audio render procedure, don't allocate memory, don't take any locks, don't waste time, printf statements for debugging only may adversly affect render you have been warned
static OSStatus renderSinewave(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
{
    MySineWavePlayer *player = (MySineWavePlayer*)inRefCon;
    double j = player->phase;
    double cycleLength = 44100. / 440.;
    int frame = 0;
    
    for (frame = 0; frame < inNumberFrames; ++frame) {
        Float32 *data = (Float32*)ioData->mBuffers[0].mData;
        (data)[frame] = (Float32)sin (2 * M_PI * (j / cycleLength));
        // copy to right channel too
        data = (Float32*)ioData->mBuffers[1].mData;
        (data)[frame] = (Float32)sin (2 * M_PI * (j / cycleLength));
        j += 1.0;
        if (j > cycleLength)
            j -= cycleLength;
    }
    
    player->phase = j;
    return noErr;
}


- (void) setupGraph{
    printf("initialize graph\n");

    AUNode outputNode;
    AUNode mixerNode;
    
    // this is the format for the graph
    mAudioFormat = [[AVAudioFormat alloc] initWithCommonFormat:AVAudioPCMFormatFloat32
                                                    sampleRate:kGraphSampleRate
                                                      channels:2
                                                   interleaved:NO];
    
    OSStatus result = noErr;
    
    // create a new AUGraph
    result = NewAUGraph(&mGraph);
    if (result) { printf("NewAUGraph result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    // create two AudioComponentDescriptions for the AUs we want in the graph
    
    // output unit
    CAComponentDescription output_desc(kAudioUnitType_Output, kAudioUnitSubType_RemoteIO, kAudioUnitManufacturer_Apple);
    CAShowComponentDescription(&output_desc);
    
    // multichannel mixer unit
    CAComponentDescription mixer_desc(kAudioUnitType_Mixer, kAudioUnitSubType_MultiChannelMixer, kAudioUnitManufacturer_Apple);
    CAShowComponentDescription(&mixer_desc);
    
    printf("new nodes\n");
    
    // create a node in the graph that is an AudioUnit, using the supplied AudioComponentDescription to find and open that unit
    result = AUGraphAddNode(mGraph, &output_desc, &outputNode);
    if (result) { printf("AUGraphNewNode 1 result %ld %4.4s\n", (long)result, (char*)&result); return; }
    
    result = AUGraphAddNode(mGraph, &mixer_desc, &mixerNode );
    if (result) { printf("AUGraphNewNode 2 result %ld %4.4s\n", (long)result, (char*)&result); return; }
    
    // connect a node's output to a node's input
    result = AUGraphConnectNodeInput(mGraph, mixerNode, 0, outputNode, 0);
    if (result) { printf("AUGraphConnectNodeInput result %ld %4.4s\n", (long)result, (char*)&result); return; }
    
    // open the graph AudioUnits are open but not initialized (no resource allocation occurs here)
    result = AUGraphOpen(mGraph);
    if (result) { printf("AUGraphOpen result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    result = AUGraphNodeInfo(mGraph, mixerNode, NULL, &mMixer);
    if (result) { printf("AUGraphNodeInfo result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    result = AUGraphNodeInfo(mGraph, outputNode, NULL, &mOutput);
    if (result) { printf("AUGraphNodeInfo result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    // set bus count: max number of files to play + the metronome bus
    UInt32 numbuses = NUMFILES + 1;
    
    printf("set input bus count %u\n", (unsigned int)numbuses);
    
    result = AudioUnitSetProperty(mMixer, kAudioUnitProperty_ElementCount, kAudioUnitScope_Input, 0, &numbuses, sizeof(numbuses));
    if (result) { printf("AudioUnitSetProperty result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    for (int i = 0; i < numbuses; ++i) {
        // setup render callback struct
        printf("set kAudioUnitProperty_SetRenderCallback for mixer input bus %d\n", i);
        AURenderCallbackStruct rcbs;
        
        // Set a callback for the specified node's specified input
        if (i == 0){
            // Set the Link metronome on bus 0
            rcbs.inputProc = &linkMetronomeProc;
            rcbs.inputProcRefCon = &_linkData;
            result = AUGraphSetNodeInputCallback(mGraph, mixerNode, i, &rcbs);
            // equivalent to AudioUnitSetProperty(mMixer, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, i, &rcbs, sizeof(rcbs));
            if (result) { printf("AUGraphSetNodeInputCallback result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
        }
        else{
            // Set the file players on remaining buses
            rcbs.inputProc = &renderInput;
            rcbs.inputProcRefCon = &_linkData;
            result = AUGraphSetNodeInputCallback(mGraph, mixerNode, i, &rcbs);
            // equivalent to AudioUnitSetProperty(mMixer, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, i, &rcbs, sizeof(rcbs));
            if (result) { printf("AUGraphSetNodeInputCallback result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
        }
        // set input stream format to what we want
        printf("set mixer input kAudioUnitProperty_StreamFormat for bus %d\n", i);
        
        result = AudioUnitSetProperty(mMixer, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, i, mAudioFormat.streamDescription, sizeof(AudioStreamBasicDescription));
        if (result) { printf("AudioUnitSetProperty result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    }
    
    // set output stream format to what we want
    printf("set output kAudioUnitProperty_StreamFormat\n");
    
    result = AudioUnitSetProperty(mMixer, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, mAudioFormat.streamDescription, sizeof(AudioStreamBasicDescription));
    if (result) { printf("AudioUnitSetProperty result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    result = AudioUnitSetProperty(mOutput, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, mAudioFormat.streamDescription, sizeof(AudioStreamBasicDescription));
    if (result) { printf("AudioUnitSetProperty result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    printf("AUGraphInitialize\n");
    
    // now that we've set everything up we can initialize the graph, this will also validate the connections
    result = AUGraphInitialize(mGraph);
    if (result) { printf("AUGraphInitialize result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    CAShow(mGraph);
}

- (void) clearBuffers{
    for (int i = 0; i < NUMFILES && i < MAXBUFS; i++)  {
        free(mSoundBuffer[i].data);
        mSoundBuffer[i].data = 0;
    }
    self.loadStatus = empty;
}

- (void) loadFiles{
    
    // don't load again if currently loading
    if (!isLoading){
        
        
        
        [self stopAUGraph];
        [self clearBuffers];
        
        
        isLoading = YES;
        self.loadStatus = loading;
        
        AVAudioFormat *clientFormat = [[AVAudioFormat alloc] initWithCommonFormat:AVAudioPCMFormatFloat32
                                                                       sampleRate:kGraphSampleRate
                                                                         channels:1
                                                                      interleaved:YES];
        
        for (int i = 0; i < NUMFILES && i < MAXBUFS; i++)  {
            printf("loadFiles, %d\n", i);
            
            // if buffer numnber is above the number URLs to load.
            if (i >= urlCount){
                mSoundBuffer[i].numFrames = 0;
                mSoundBuffer[i].data = (Float32 *)calloc(0, sizeof(Float32));
                mSoundBuffer[i].sampleNum = 0;
            }
            else {
                
                ExtAudioFileRef xafref = 0;
                
                // open one of the two source files
                NSLog(@" source URL %@", (__bridge NSURL*) sourceURL[i]);
                
                OSStatus result = ExtAudioFileOpenURL(sourceURL[i], &xafref);
                
                if (result || !xafref) {
                    printf("ExtAudioFileOpenURL result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result);
                    
                    // If no file is found, set with 0
                    mSoundBuffer[i].numFrames = 0;
                    mSoundBuffer[i].data = (Float32 *)calloc(0, sizeof(Float32));
                    mSoundBuffer[i].sampleNum = 0;
                    
                } else {
                    
                    // get the file data format, this represents the file's actual data format
                    AudioStreamBasicDescription fileFormat;
                    UInt32 propSize = sizeof(fileFormat);
                    
                    result = ExtAudioFileGetProperty(xafref, kExtAudioFileProperty_FileDataFormat, &propSize, &fileFormat);
                    if (result) { printf("ExtAudioFileGetProperty kExtAudioFileProperty_FileDataFormat result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); break; }
                    
                    // set the client format - this is the format we want back from ExtAudioFile and corresponds to the format
                    // we will be providing to the input callback of the mixer, therefore the data type must be the same
                    
                    // used to account for any sample rate conversion
                    double rateRatio = kGraphSampleRate / fileFormat.mSampleRate;
                    
                    propSize = sizeof(AudioStreamBasicDescription);
                    result = ExtAudioFileSetProperty(xafref, kExtAudioFileProperty_ClientDataFormat, propSize, clientFormat.streamDescription);
                    if (result) { printf("ExtAudioFileSetProperty kExtAudioFileProperty_ClientDataFormat %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); break; }
                    
                    // get the file's length in sample frames
                    UInt64 numFrames = 0;
                    propSize = sizeof(numFrames);
                    result = ExtAudioFileGetProperty(xafref, kExtAudioFileProperty_FileLengthFrames, &propSize, &numFrames);
                    if (result) { printf("ExtAudioFileGetProperty kExtAudioFileProperty_FileLengthFrames result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); break; }
                    printf("File %d, Number of Sample Frames: %u\n", i, (unsigned int)numFrames);
                    
                    numFrames = (numFrames * rateRatio); // account for any sample rate conversion
                    printf("File %d, Number of Sample Frames after rate conversion (if any): %u\n", i, (unsigned int)numFrames);
                    
                    // set up our buffer
                    mSoundBuffer[i].numFrames = (UInt32)numFrames;
                    mSoundBuffer[i].asbd = *(clientFormat.streamDescription);
                    
                    UInt32 samples = (UInt32)numFrames * mSoundBuffer[i].asbd.mChannelsPerFrame;
                    mSoundBuffer[i].data = (Float32 *)calloc(samples, sizeof(Float32));
                    mSoundBuffer[i].sampleNum = 0;
                    
                    // set up a AudioBufferList to read data into
                    AudioBufferList bufList;
                    bufList.mNumberBuffers = 1;
                    bufList.mBuffers[0].mNumberChannels = 1;
                    bufList.mBuffers[0].mData = mSoundBuffer[i].data;
                    bufList.mBuffers[0].mDataByteSize = samples * sizeof(Float32);
                    
                    // perform a synchronous sequential read of the audio data out of the file into our allocated data buffer
                    UInt32 numPackets = (UInt32)numFrames;
                    result = ExtAudioFileRead(xafref, &numPackets, &bufList);
                    if (result) {
                        printf("ExtAudioFileRead result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result);
                        free(mSoundBuffer[i].data);
                        mSoundBuffer[i].data = 0;
                    }
                    
                    // relase the retained sourceURL ref
                    CFRelease(sourceURL[i]);
                }
                // close the file and dispose the ExtAudioFileRef
                ExtAudioFileDispose(xafref);
            }
        }
        _linkData.audioBuffersPtr = mSoundBuffer;
        
        // sequence
        
        isLoading = NO;
        //        [clientFormat release];
        
        self.loadStatus = loaded;
    }
}

#pragma mark- API
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[AudioManager alloc] init];
        [_sharedInstance initVars];
        [_sharedInstance setupSession];
        [_sharedInstance setupGraph];
    });
    return _sharedInstance;
}

- (void) loadTestFiles{
    // create the URLs we'll use for source A and B
    NSString *sourceA = [[NSBundle mainBundle] pathForResource:@"theorbe_impair" ofType:@"mp3"];
    NSString *sourceB = [[NSBundle mainBundle] pathForResource:@"theorbe_pair" ofType:@"mp3"];
    
    sourceURL[0] = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)sourceA, kCFURLPOSIXPathStyle, false) ;
    sourceURL[1] = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)sourceB, kCFURLPOSIXPathStyle, false) ;
    [self performSelectorInBackground:@selector(loadFiles) withObject:nil];
}

- (void) loadAudio:(NSURL*)oddURL evenFile:(NSURL*)evenURL oddURLs:(NSArray*)oddURLs  evenURLs:(NSArray*)evenURLs{
    
    // I use __bridge_retained here because else, the reference to the URLs is deallocated in the loadFiles() call
    // see http://stackoverflow.com/questions/13770040/how-to-copy-a-nsurl-object-to-an-array-of-cfurl-structs to see an explanation
    
    
    sourceURL[0] = (__bridge_retained CFURLRef) oddURL;
    sourceURL[1] = (__bridge_retained CFURLRef) evenURL;
    
    urlCount = 2;
    
    int nextOddUrlIndex = 2;
    int nextEvenUrlIndex = 3;
    for (int i = 0; i < oddURLs.count; i++) {
        NSLog(@"i %d", i);
        NSLog(@"%@",(NSURL*) [oddURLs objectAtIndex:i]);
        sourceURL[nextOddUrlIndex] = (__bridge_retained CFURLRef) [oddURLs objectAtIndex:i];
        sourceURL[nextEvenUrlIndex] = (__bridge_retained CFURLRef) [evenURLs objectAtIndex:i];
        nextOddUrlIndex += 2;
        nextEvenUrlIndex += 2;
        urlCount += 2;
    }
    
    [self performSelectorInBackground:@selector(loadFiles) withObject:nil];
}

- (Float64)bpm {
    return ABLLinkGetTempo(ABLLinkCaptureAppTimeline(_linkData.ablLink));
}

- (void)setBpm:(Float64)bpm {
    OSSpinLockLock(&lock);
    _linkData.sharedEngineData.proposeBpm = bpm;
    OSSpinLockUnlock(&lock);
}

- (Float64)beatTime {
    return ABLLinkBeatAtTime(
                             ABLLinkCaptureAppTimeline(_linkData.ablLink),
                             mach_absolute_time(),
                             self.quantum);
}
- (Float64)getBarPhase {
    return ABLLinkPhaseAtTime(
                              ABLLinkCaptureAppTimeline(_linkData.ablLink),
                              mach_absolute_time(),
                              self.quantum);
}
- (int)getBarIndex {
    return (int) _linkData.barIndex;
}


- (void) setBarIndex:(int)index{
    if (![self isGraphPlaying]){
        _linkData.barIndex = index;
    }
}

- (Float64)quantum {
    return _linkData.sharedEngineData.quantum;
}

- (void)setQuantum:(Float64)quantum {
    OSSpinLockLock(&lock);
    _linkData.sharedEngineData.quantum = quantum;
    OSSpinLockUnlock(&lock);
}

- (void)setInstrument:(enum instrument)instrument{
    OSSpinLockLock(&lock);
    _linkData.sharedEngineData.instrument = instrument;
    OSSpinLockUnlock(&lock);
    
}

// Set the number of bars in loop, ie the lenght of the variation sequence
- (void)setSequenceSize:(int)size {
    OSSpinLockLock(&lock);
    _linkData.sequenceSize = size;
    OSSpinLockUnlock(&lock);
}

- (void)setVariation:(int)variation atSlot:(int)slotIndex forPart:(int)partIndex {
    if (slotIndex < _linkData.sequenceSize){
        OSSpinLockLock(&lock);
        _linkData.sharedEngineData.sequence[partIndex][slotIndex] = variation;
        OSSpinLockUnlock(&lock);
    }
}

- (void)soloContinuoPart:(int)partIndex {
    OSSpinLockLock(&lock);
    for (int i = 2; i < NUMFILES; i++){
        if ((i == (2 * partIndex) + 2) || (i == (2 * partIndex) + 3)){
            _linkData.sharedEngineData.soloPart[i] = 1;
        } else {
            _linkData.sharedEngineData.soloPart[i] = 0;
        }
    }
    OSSpinLockUnlock(&lock);
}

- (void)unsoloContinuoParts{
    OSSpinLockLock(&lock);
    for (int i = 0; i < NUMFILES; i++){
       _linkData.sharedEngineData.soloPart[i] = 1;
    }
    OSSpinLockUnlock(&lock);
}


- (BOOL)isLinkEnabled {
    return ABLLinkIsEnabled(_linkData.ablLink);
}

- (ABLLinkRef)linkRef {
    return _linkData.ablLink;
}

// sets the input volume for a specific bus
- (void)setInputVolume:(UInt32)inputNum value:(AudioUnitParameterValue)value
{
    OSStatus result = AudioUnitSetParameter(mMixer, kMultiChannelMixerParam_Volume, kAudioUnitScope_Input, inputNum, value, 0);
    if (result) { printf("AudioUnitSetParameter kMultiChannelMixerParam_Volume Input result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
}

// start render
- (void)startAUGraph
{
    if (!isLoading){
        printf("PLAY\n");
        
        OSStatus result = AUGraphStart(mGraph);
        if (result) { printf("AUGraphStart result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
        _isGraphPlaying = YES;
        
        OSSpinLockLock(&lock);
        _linkData.sharedEngineData.isPlaying = _isGraphPlaying;
        if (_isGraphPlaying) {
            _linkData.sharedEngineData.resetToBeatTime = 0;
        }
        _linkData.sharedEngineData.playStatus = armed;
        
        OSSpinLockUnlock(&lock);
        
    } else {
        printf("Player is loading buffers\n");
    }
}

// stops render
- (void)stopAUGraph
{
    printf("STOP\n");
    
    Boolean isRunning = false;
    
    OSStatus result = AUGraphIsRunning(mGraph, &isRunning);
    if (result) { printf("AUGraphIsRunning result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
    
    if (isRunning) {
        result = AUGraphStop(mGraph);
        if (result) { printf("AUGraphStop result %ld %08lX %4.4s\n", (long)result, (long)result, (char*)&result); return; }
        _isGraphPlaying = NO;
    }
}


@end
