//
//  Extensions.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hexString:Int) {
        self.init(red:(hexString >> 16) & 0xff, green:(hexString >> 8) & 0xff, blue:hexString & 0xff)
    }
}


// From http://stackoverflow.com/questions/24857986/load-a-uiview-from-nib-in-swift
extension UIView {
    @discardableResult
    func fromNib<T : UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {
            // xib not loaded, or it's top view is of the wrong type
            return nil
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.layoutAttachAll(to: self)
        return view
    }
}


extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
    // style the border to show as a grey underline
    func borderAsGreyUnderline(){
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor(hexString: lightGrey).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}


extension UILabel{
    func partialUnderline(){
        let border = CALayer()
        let width = CGFloat(1.0)
        let lineCenter = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height * 1.5)
        let lineLength = CGFloat(self.frame.size.width / 4)
        
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: lineCenter.x - lineLength/2, y: self.frame.size.height - width, width:  lineLength, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }}
