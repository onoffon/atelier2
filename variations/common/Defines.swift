//
//  Defines.swift
//  variations
//
//  Created by julien@macmini on 19/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import UIKit

enum PlayMode : Int {
    case onePlayer
    case multiplePlayers
}

enum Instrument : Int {
    case solo
    case continuo
    case all
}

enum PlayerState : Int {
    case playing
    case notPlaying
}

// Default variation names
let defaultVariationNames: [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T"]

let numberOfPresetSlots: Int = 8

//let numberOfVariations:Int = variationNames.count
//let headerSize: CGFloat = 80

// color palette for variations
let candycane: [Int] = [0x325D7F,
                           0x3AAFA9,
                           0x84AF9C,
                           0xA2D5AC,
                           0x6D5C7E,
                           0xC06C86,
                           0xF2727F,
                           0xF69B9A,
                           0xF04F54,
                           0xF6903C,
                           0xF7D420,
                           0xECE674]

let blackBlue: Int = 0x32323C
let grey: Int = 0x919199
let lightGrey: Int = 0xE1E1EB
let red: Int = 0xF04F54

/// precomputed value of squareRoot(0.5)
let sqrt_half = 0.5.squareRoot()

// The default text style attributes for the info pages
func infoTextAttributes()->  NSDictionary? {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = 20
    paragraphStyle.alignment = .justified
    
    let attributes : NSDictionary? = [NSParagraphStyleAttributeName : paragraphStyle,
                                      NSForegroundColorAttributeName: UIColor.white,
                                      NSFontAttributeName: UIFont(name: "Frutiger", size: 20.0)!
    ]
    return attributes
}
