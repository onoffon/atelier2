//
//  AudioManager.h
//  variations
//
//  Created by julien@macmini on 10/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

#ifndef AudioManager_h
#define AudioManager_h

#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVAudioFormat.h>
#import <Foundation/Foundation.h>
#include "ABLLink.h"

#define MAXBUFS  10
#define NUMFILES 10
#define MAXSEQUENCESIZE 64
#define MAXCONTINUOPARTS 4




typedef struct {
    AudioStreamBasicDescription asbd;
    Float32 *data;
    UInt32 numFrames;
    UInt32 sampleNum;
    
} SoundBuffer, *SoundBufferPtr;

typedef struct MySineWavePlayer {
    double phase;
} MySineWavePlayer;

enum instrument {solo, continuo, all};

enum LoadStatus {empty, loading, loaded};

enum PlayStatus {stopped, armed, playing};

/*
 * Structure that stores engine-related data that can be changed from
 * the main thread.
 */
typedef struct {
    UInt32 outputLatency; // Hardware output latency in HostTime
    Float64 resetToBeatTime;
    Float64 proposeBpm;
    Float64 quantum;
    BOOL isPlaying;
    UInt8 sequence[MAXCONTINUOPARTS][MAXSEQUENCESIZE];
    enum instrument instrument;
    enum PlayStatus playStatus;
    UInt8 soloPart[NUMFILES];
    
} EngineData;

/*
 * Structure that stores all data needed by the audio callback.
 */
typedef struct {
    ABLLinkRef ablLink;
    // Shared between threads. Only write when engine not running.
    Float64 sampleRate;
    // Shared between threads. Only write when engine not running.
    Float64 secondsToHostTime;
    // Shared between threads. Written by the main thread and only
    // read by the audio thread when doing so will not block.
    EngineData sharedEngineData;
    // Copy of sharedEngineData owned by audio thread.
    EngineData localEngineData;
    // Owned by audio thread
    UInt64 timeAtLastClick;
    UInt64 timeAtLastBar;
    UInt64 barPhaseAtLastClick;
    UInt8 barIndex;
    UInt8 sequenceSize;
    double currentTempo;
    // AudioFile buffers
    SoundBufferPtr audioBuffersPtr;
    
} LinkData;

@interface AudioManager : NSObject
{
    
    CFURLRef sourceURL[NUMFILES];
    
    AVAudioFormat *mAudioFormat;
    
    AUGraph   mGraph;
    AudioUnit mMixer;
    AudioUnit mOutput;
    
    SoundBuffer mSoundBuffer[MAXBUFS];
    
    Boolean isPlaying;
    Boolean isLoading;
    
    int urlCount;
    
    MySineWavePlayer mSinewaveData;
}


@property (readonly, nonatomic) Boolean isGraphPlaying;
@property (nonatomic) BOOL playLoop;
+ (instancetype) sharedInstance;
- (void)loadAudio:(NSURL*)oddURL evenFile:(NSURL*)evenURL oddURLs:(NSArray*)oddURLs  evenURLs:(NSArray*)evenURLs;
- (void)setInputVolume:(UInt32)inputNum value:(AudioUnitParameterValue)value;
- (void)setOutputVolume:(AudioUnitParameterValue)value;

- (void)startAUGraph;
- (void)stopAUGraph;
- (void)clearBuffers;
- (void)loadTestFiles;
- (void)setSequenceSize:(int)size;
- (void)setVariation:(int)variation atSlot:(int)slotIndex forPart:(int)partIndex;
- (void)setInstrument:(enum instrument)instrument;
- (int) getBarIndex;
- (void)setBarIndex:(int)index;
- (Float64) getBarPhase;
- (void)soloContinuoPart:(int)partIndex;
- (void)unsoloContinuoParts;

// Link support
@property (nonatomic) Float64 bpm;
@property (readonly, nonatomic) Float64 beatTime;

@property (nonatomic) Float64 quantum;
@property (readonly, nonatomic) BOOL isLinkEnabled;
@property (readonly, nonatomic) ABLLinkRef linkRef;

@property enum LoadStatus loadStatus;

@end


#endif /* AudioManager_h */
