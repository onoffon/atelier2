//
//  UIButtonTitle.swift
//  variations
//
//  Created by Julien Bloit on 4/9/17.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation
import UIKit

class UIButtonTitle: UIButton{
    
        var border: CALayer = CALayer()
        var width: CGFloat = CGFloat(2.0)

    override var isSelected: Bool{
        didSet{
            self.backgroundColor = UIColor.white
            
            if isSelected{
                self.border.isHidden = false
            } else {
                self.border.isHidden = true
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }

    func customInit(){

        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
    }

    func setUnderlinerColor(color: UIColor){
        border.borderColor = color.cgColor
    }

}
