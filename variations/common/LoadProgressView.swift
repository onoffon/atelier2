//
//  LoadProgressView.swift
//  variations
//
//  Created by julien@macmini on 19/02/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//


// View to display while user is waiting.

import Foundation
import UIKit


enum progressDisplayStyle {
    case spinner
    case progressBar
}


@IBDesignable
class LoadProgressView: UIView {
    
    var title: UILabel!
    var spinner: UIActivityIndicatorView!
    var width: CGFloat!
    var height: CGFloat!
    var progressBar: UIProgressView!
    
    init(frame: CGRect, style: progressDisplayStyle = .spinner)  {
        super.init(frame: frame);
        self.customInit(style: style);
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.customInit(style: .spinner);
    }
    
    // only called within xcode's interface builder. Useful for designable.
    override func prepareForInterfaceBuilder() {
        customInit(style: .spinner)
    }
    
    func customInit(style: progressDisplayStyle) {
        self.backgroundColor = UIColor.clear
        width = frame.size.width
        height = frame.size.height
        let dummyTempFrame = CGRect(x: 0, y: 0, width: 100, height: 100) // real layout will happen in performLayout()
        let dummyTempRadius: CGFloat = 20.0
        title = UILabel(frame: dummyTempFrame)
        title.text = ""
        title.textColor = UIColor(hexString: blackBlue)
        
        
        spinner = UIActivityIndicatorView()
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        spinner.color = UIColor(hexString: blackBlue)
        spinner.center = self.center

        progressBar = UIProgressView(progressViewStyle: .default)
        progressBar.center = self.center

        switch style {
        case .progressBar:
            self.addSubview(progressBar)
        default:
            self.addSubview(spinner)
        }
        
        self.addSubview(title)

    }
    
    // Called after view is loaded with view size updated from autolayout constraints.
    override func layoutSubviews() {
        performLayout()
    }
    
    func performLayout(){
        title.center = self.center
    
        spinner.center = self.center
        spinner.center.y = title.center.y + 50
        spinner.startAnimating()


        title.font = UIFont(name: "Frutiger-Roman", size: 20)
        title.adjustsFontSizeToFitWidth = true

        title.frame.size.width = self.width/2
        title.textAlignment = NSTextAlignment.center
        title.center.x = self.center.x
        
        progressBar.center = self.center
        progressBar.center.y = title.center.y + 50
        progressBar.progressTintColor = UIColor(hexString: red)

        
    }
    
    func setTitle(titleText: String){
        title.text = titleText
    }
    
    func setProgress(progress: Float){
        if let bar = progressBar {
            bar.setProgress(progress, animated: true)
        }
    }
    
}
