//
//  DataManager.swift
//  variations
//
//  Created by julien@macmini on 06/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import Firebase

// I think of this class as a store clerck, responsible for supplying stocks, maintaining a catalog, and serving the data.


// enum syntax to allow KVO, from http://stackoverflow.com/questions/27292918/swift-kvo-observing-enum-properties
@objc
enum CatalogStatus : Int {
    case notInitialized
    case emptyCatalog
    case updatingCatalog
    case filledCatalog
    case downloading
    case doneDownloading
}

class DataManager: NSObject {
    
    // Singleton
    static let sharedInstance = DataManager()
    
    // ---- Properties
    
    /// Country code for user's language settings. Base is French
    var userLanguageCode: String = "fr"
    
    // Firebase storage and database
    var storage = Storage.storage()
    let databaseRef = Database.database().reference(withPath: "pieces")
    
    //The list of available pieces
    var catalog: [Piece] = []
    
    // Number of data items referenced in the catalog that need to be downloaded
    var itemCount: Int = 0
    var downloadedCount: Int = 0 {
        didSet{
            print(downloadedCount)
            if (downloadedCount == itemCount){
                self.catalogStatus = .doneDownloading
                print("downloadComplete")
            }
        }
    }
    
    // Local storage
    let audioFolderPath: URL
    let imageFolderPath: URL
    let textFolderPath: URL
    
    // Status
    dynamic var catalogStatus: CatalogStatus = .notInitialized

    //MARK:- LIFECYCLE
    override init() {

        // Detect language code from settings
        if let languageCode  = NSLocale.current.languageCode {
            self.userLanguageCode = languageCode
        }
        
        // Make sure there are audio and image folders
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        audioFolderPath = documentsDirectory.appendingPathComponent("audio", isDirectory: true)
        imageFolderPath = documentsDirectory.appendingPathComponent("image", isDirectory: true)
        textFolderPath = documentsDirectory.appendingPathComponent("text", isDirectory: true)
        
        do {
            try FileManager.default.createDirectory(atPath: audioFolderPath.absoluteString, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        do {
            try FileManager.default.createDirectory(atPath: imageFolderPath.absoluteString, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        do {
            try FileManager.default.createDirectory(atPath: textFolderPath.absoluteString, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
    }
    
    // Update the catalog, check data supplies, set status
    func setup(){
        databaseRef.observe(.value, with: { snapshot in
            self.catalogStatus = .updatingCatalog
            var newCatalog: [Piece] = []
            for item in snapshot.children {
                let piece = Piece(snapshot: item as! DataSnapshot)
                newCatalog.append(piece)
            }
            self.catalog = newCatalog
            self.catalogStatus = .filledCatalog
            
            self.getTotalItemCount()
            
            self.catalogStatus = .downloading
            
            for piece in self.catalog {
                self.fetchPieceData(piece)
            }
        })
    }
    
    //MARK:- API
    func getDownloadProgress() -> (count: Int, total: Int)? {
        var returnTuple: (Int, Int)? = nil
        if self.catalog.count > 0 {
            returnTuple = (self.downloadedCount, self.itemCount)
        }
        return returnTuple
    }
    
    
    //MARK:- HELPERS
    
    /**
     Sets the the itemCount property with the number of items to be downloaded for the whole catalog.
     */
    private func getTotalItemCount() {
        for piece in self.catalog {
            // count 2 for solo
            itemCount += 2
            
            // count 2 per continuo part
            for _ in piece.continuo! {
                itemCount += 2
            }
            
            // count a background image file
            itemCount += 1
            
            // count a text file for the info page
            itemCount += 1
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    // check whether a piece has all its data locally
    func pieceHasLocalData(_ piece: Piece) -> Bool {
        
        var pieceHasData = true
        pieceHasData = (pieceHasData && partHasLocalData(piece.solo!))
        
        for continuoPart in piece.continuo! {
            pieceHasData = (pieceHasData && partHasLocalData(continuoPart))
        }
        return pieceHasData
    }
    
    func partHasLocalData(_ part: InstrumentPart) -> Bool {

            //TODO: check local file with hash, to make sure it's the right version
            var partHasData = true
        
            // --- ODD BARS
            let odd_audioURL: URL = audioFolderPath.appendingPathComponent((part.oddBarsLocalReference)!, isDirectory: false)
            if (FileManager.default.fileExists(atPath: odd_audioURL.path)){
                print("File \(odd_audioURL.path) already on device")
            } else {
                partHasData = false
            }
            
            // --- EVEN BARS
            let even_audioURL = audioFolderPath.appendingPathComponent((part.evenBarsLocalReference)!, isDirectory: false)
            
            if (FileManager.default.fileExists(atPath: even_audioURL.path)){
                print("File \(even_audioURL.path) already on device")
            } else {
                partHasData = false
            }
        
        return partHasData
    }
    
    // Download data from the a DB Piece record
    func fetchPieceData(_ piece: Piece){
        
        print("-------------- data to retrieve")
        print(String(describing: piece.title))
        print(piece.key)
        
        downloadPieceImage(piece: piece)
        downloadPieceText(piece: piece)
        downloadPart(piece.solo!)
        
        for continuoPart in piece.continuo! {
            downloadPart(continuoPart)
        }
    }
    
    func downloadPart(_ part: InstrumentPart){
        
        //TODO: check local file with hash, to make sure it's the right version
        
        // --- ODD BARS
        // Get the online storage reference
        let odd_audioStoragePath = (part.bucketURL)! + "/" + (part.oddBarsAudioFileName)!
        let odd_audioStorageRef  = storage.reference(forURL: odd_audioStoragePath)
        
        // Download to the local filesystem, if file is not already on device
        let odd_audioURL: URL = audioFolderPath.appendingPathComponent((part.oddBarsLocalReference)!, isDirectory: false)
        
        if (FileManager.default.fileExists(atPath: odd_audioURL.path)){
            print("File \(odd_audioURL.path) already on device")
            downloadedCount += 1
            
        } else {
            let odd_downloadTask = odd_audioStorageRef.write(toFile: odd_audioURL)
            odd_downloadTask.observe(.progress) { (snapshot) -> Void in
                // Download reported progress
                
                print((snapshot.progress?.completedUnitCount)!)
                print((snapshot.progress?.totalUnitCount)!)
                
            }
            odd_downloadTask.observe(.success) { (snapshot) -> Void in
                self.downloadedCount += 1
            }
        }
        
        // --- EVEN BARS
        // Get the online storage reference
        let even_audioStoragePath = (part.bucketURL)! + "/" + (part.evenBarsAudioFileName)!
        let even_audioStorageRef  = storage.reference(forURL: even_audioStoragePath)
        
        // Download to the local filesystem, if file is not already on device
        let even_audioURL = audioFolderPath.appendingPathComponent((part.evenBarsLocalReference)!, isDirectory: false)
        
        if (FileManager.default.fileExists(atPath: even_audioURL.path)){
            print("File \(even_audioURL.path) already on device")
            downloadedCount += 1
        } else {
            let even_downloadTask = even_audioStorageRef.write(toFile: even_audioURL)
            even_downloadTask.observe(.progress) { (snapshot) -> Void in
                // Download reported progress
                print((snapshot.progress?.completedUnitCount)!)
                print((snapshot.progress?.totalUnitCount)!)
            }
            even_downloadTask.observe(.success) { (snapshot) -> Void in
                self.downloadedCount += 1
            }
        }
    }
    
    func downloadPieceImage(piece: Piece){

        // Get the online storage reference
        let imageStoragePath = (piece.imageBucketURL)! + "/" + (piece.imageFileName)!
        let imageStorageRef  = storage.reference(forURL: imageStoragePath)
        
        // Download to the local filesystem, if file is not already on device
        let imageURL: URL = imageFolderPath.appendingPathComponent((piece.imageLocalReference)!, isDirectory: false)
        
        if (FileManager.default.fileExists(atPath: imageURL.path)){
            print("File \(imageURL.path) already on device")
            downloadedCount += 1
            
        } else {
            let image_downloadTask = imageStorageRef.write(toFile: imageURL)
            image_downloadTask.observe(.progress) { (snapshot) -> Void in
                // Download reported progress
                
                print((snapshot.progress?.completedUnitCount)!)
                print((snapshot.progress?.totalUnitCount)!)
                
            }
            image_downloadTask.observe(.success) { (snapshot) -> Void in
                self.downloadedCount += 1
            }
        }
    }
    
    func downloadPieceText(piece: Piece){
        
        // Get the online storage reference
        let textStoragePath = (piece.infotextBucketURL)! + "/" + (piece.infotextFileName)!
        let textStorageRef  = storage.reference(forURL: textStoragePath)
        
        // Download to the local filesystem, if file is not already on device
        let textURL: URL = textFolderPath.appendingPathComponent((piece.infotextLocalReference)!, isDirectory: false)
        
        if (FileManager.default.fileExists(atPath: textURL.path)){
            print("File \(textURL.path) already on device")
            downloadedCount += 1
            
        } else {
            let text_downloadTask = textStorageRef.write(toFile: textURL)
            text_downloadTask.observe(.progress) { (snapshot) -> Void in
                // Download reported progress
                
                print((snapshot.progress?.completedUnitCount)!)
                print((snapshot.progress?.totalUnitCount)!)
                
            }
            text_downloadTask.observe(.success) { (snapshot) -> Void in
                self.downloadedCount += 1
            }
        }
    }
}
