//
//  Student.swift
//  variations
//
//  Created by julien@macmini on 11/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation
import Firebase

// a student in a class, and his data

struct Student {

    let id: String!
    let name: String?
    var presets: [PiecePreset]!
    
    init(){
        id = ""
        name = ""
        presets = []
    }
    
    init(name: String
        ){
        id = ""
        self.name = name
        presets = []
    }

    init(snapshot: DataSnapshot){
        id = snapshot.key
        let value = snapshot.value as? NSDictionary
        name = value?["name"] as! String?
        presets = []
        //TODO: populate presets from snapshot
        
    }
    
    // return as a dict for DB insert
    func toAnyObject() -> Any {
        
        var presetsDict: [String: Any] = [:]
        for preset in presets{
            presetsDict[preset.pieceId] = preset.toAnyObject()
        }
        return [
            "name": self.name!,
            "presets": presetsDict
        ]
    }
    
}
