//
//  Class.swift
//  variations
//
//  Created by julien@macmini on 05/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation
import Firebase

// A "Class" of students

struct Class {
    let id: String!
    let ownerId: String!
    let className: String?
    let schoolName: String?
    let students: [Student]?
    
    init(){
        self.id = nil
        self.ownerId = nil   // id for current logged in user
        self.className = ""
        self.schoolName = ""
        self.students = []
        
    }
    
    init(id: String,
         ownerId: String,
         className: String,
         schoolName: String
        ){
        self.id = id
        self.ownerId = ownerId
        self.className = className
        self.schoolName = schoolName
        self.students = []
    }
    
    
    init(snapshot: DataSnapshot){
        let value = snapshot.value as? NSDictionary
        self.id = snapshot.key
        self.ownerId = value?["ownerId"] as? String ?? ""
        self.className = value?["className"] as? String ?? ""
        self.schoolName = value?["schoolName"] as? String ?? ""
        self.students = []

        let studentsSnapshot = snapshot.childSnapshot(forPath: "students")
        for studentSnapshot in studentsSnapshot.children{
            let student = Student(snapshot: studentSnapshot as! DataSnapshot)
            students?.append(student)
        }
        print("made class form snapshot")

    }
    
    init(id: String, ownerId: String, dict: [String: Any]) {
        self.id = id
        self.ownerId = ownerId
        self.className = dict["className"] as? String
        self.schoolName = dict["schoolName"] as? String
        
        let studentsDict = dict["students"]
//        for student in studentsDict.
        self.students = []
    
    }
    
    
    // return as a dict for DB insert
    func toAnyObject() -> Any {
        return [
            "ownerId": self.ownerId,
            "className": self.className,
            "schoolName": self.schoolName,
        ]
    }
}
