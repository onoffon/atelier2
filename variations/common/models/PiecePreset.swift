//
//  PiecePreset.swift
//  variations
//
//  Created by julien@macmini on 23/01/2017.
//  Copyright © 2017 jbloit. All rights reserved.
//

import Foundation
import Firebase

typealias variationPattern = [[Int]]

struct PartPreset {
    var sequences: [[Int]] // dim 1: numberOfPresetSlots - dim2: numberOfBarsInLoop
    var variationNames: [String] // dim 1: numberOfLoopsInContinuoPart
    var variationPatterns: [[[Int]]] // dim 1: numberOfLoopsInContinuoPart, dim 2: pattern line dim 3: pattern dot
}

class PiecePreset: NSObject{
    
    // the piece Id for which this preset is saved
    var pieceId: String
    
    
    // Data for each different part
    var solo: PartPreset
    var continuo: [PartPreset]
    
    var defaultPattern: [[Int]] = [[-1, -1]] // can't be empty or else the whole variationPattern node is not written in DB
    
    /// prepopulate preset data with default values
    init(forPiece: Piece){
        self.pieceId = forPiece.key
        
        let numberOfBarsInLoop = forPiece.numberOfBarsInLoop!
        let numberOfLoopsInSolo = (forPiece.solo?.numberOfLoops)!
        
        self.solo = PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                               variationNames: Array(defaultVariationNames[0..<numberOfLoopsInSolo]),
                               variationPatterns: Array(repeating: defaultPattern, count: numberOfLoopsInSolo))
        self.continuo = []
        if let continuoParts = forPiece.continuo {
            for part in continuoParts {
                let numberOfLoopsInContinuoPart = part.numberOfLoops!
                let continuoPreset = PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                                                variationNames: Array(defaultVariationNames[0..<numberOfLoopsInContinuoPart]),
                                                variationPatterns: Array(repeating: defaultPattern, count: numberOfLoopsInContinuoPart))
                
                self.continuo.append(continuoPreset)
            }
        }
    }
    
    
    init(pieceId: String){
        self.pieceId = pieceId
        solo = PartPreset(sequences: [], variationNames: [], variationPatterns: [])
        continuo = [PartPreset(sequences: [], variationNames: [], variationPatterns: [])]
    }
    
    /// prepopulate preset data with default values
    init(pieceId: String, numberOfBarsInLoop: Int){
        self.pieceId = pieceId
        solo = PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                          variationNames: defaultVariationNames,
                          variationPatterns: Array(repeating: defaultPattern, count: numberOfPresetSlots))
        continuo = [PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                              variationNames: defaultVariationNames,
                              variationPatterns: Array(repeating: defaultPattern, count: numberOfPresetSlots))]
    }
    
    
    /// prepopulate preset data with default values
    init(pieceId: String, numberOfBarsInLoop: Int, numberOfLoopsInSolo: Int, numberOfLoopsInContinuo: Int){
        self.pieceId = pieceId
        solo = PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                          variationNames: Array(defaultVariationNames[0..<numberOfLoopsInSolo]),
                          variationPatterns: Array(repeating: defaultPattern, count: numberOfLoopsInSolo))
        continuo = [PartPreset(sequences: Array(repeating: Array(repeating: 0, count: numberOfBarsInLoop), count: numberOfPresetSlots),
                              variationNames: Array(defaultVariationNames[0..<numberOfLoopsInContinuo]),
                              variationPatterns: Array(repeating: defaultPattern, count: numberOfLoopsInContinuo))]
    }
    
    // recall data from snapshot
    init(snapshot: DataSnapshot) {

        pieceId = snapshot.key
        
        solo = PartPreset(sequences: snapshot.childSnapshot(forPath: "solo/sequences").value as! [[Int]],
                          variationNames: snapshot.childSnapshot(forPath: "solo/variationNames").value as! [String],
                          variationPatterns: snapshot.childSnapshot(forPath: "solo/variationPatterns").value as! [[[Int]]])
        let continuoParts = snapshot.childSnapshot(forPath: "continuo").value as! [Any]

        continuo = []
        for (i, part) in continuoParts.enumerated() {
            let partPreset = PartPreset(sequences: snapshot.childSnapshot(forPath: "continuo/\(i)/sequences").value as! [[Int]],
                                        variationNames: snapshot.childSnapshot(forPath: "continuo/\(i)/variationNames").value as! [String],
                                        variationPatterns: snapshot.childSnapshot(forPath: "continuo/\(i)/variationPatterns").value as! [[[Int]]])
            continuo.append(partPreset)
        }
    }
    
    // return as a dict for DB insert
    func toAnyObject() -> Any {
        
        var returnDict: [String: Any] = [:]
        
        let soloDict = partToDict(withPart: solo)
        returnDict["solo"] = soloDict

        var continuoDict: [String: Any] = [:]
        var partDictArray: [[String: Any]] = []
        
        for partPreset in self.continuo {
            partDictArray.append(partToDict(withPart: partPreset))
        }
        continuoDict["parts"] = partDictArray
        
        returnDict["continuo"] = partDictArray
        
        return returnDict
        
    }

    // helper converter
    func partToDict(withPart: PartPreset) -> [String: Any]{
        
        var returnDict : [String: Any] = [:]
        
        returnDict["sequences"] = withPart.sequences
        returnDict["variationNames"] = withPart.variationNames
        returnDict["variationPatterns"] = withPart.variationPatterns
        
        return returnDict
    
    }
    
}
