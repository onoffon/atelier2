//
//  InstrumentPart.swift
//  variations
//
//  Created by julien@macmini on 07/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation

// For Exemple
//"instrumentName": "Violon",
//"numberOfLoops": 11,
//"bucketURL": "gs://talenschool-composer-fee59.appspot.com/folia",
//"oddBarsAudioFileName": "violon_impair.mp3",
//"evenBarsAudioFileName": "violon_pair.mp3"


struct InstrumentPart {
    let pieceID: String?
    let instrumentName: String?
    let numberOfLoops: Int?
    let bucketURL: String?
    let oddBarsAudioFileName: String?
    let evenBarsAudioFileName: String?
    
    // a unique name to store the file locally
    let oddBarsLocalReference: String?
    let evenBarsLocalReference: String?
    
    
    init(){
        self.pieceID = nil
         self.instrumentName = ""
         self.numberOfLoops = 0
         self.bucketURL = ""
         self.oddBarsAudioFileName = ""
         self.evenBarsAudioFileName = ""
        self.oddBarsLocalReference = nil
        self.evenBarsLocalReference = nil
    }
    
    init(pieceID: String,
        instrumentName: String,
         numberOfLoops: Int,
         bucketURL: String,
         oddBarsAudioFileName: String,
         evenBarsAudioFileName: String,
         oddBarsLocalReference: String,
         evenBarsLocalReference: String
         ){
        self.pieceID = pieceID
        self.instrumentName = instrumentName
        self.numberOfLoops = numberOfLoops
        self.bucketURL = bucketURL
        self.oddBarsAudioFileName = oddBarsAudioFileName
        self.evenBarsAudioFileName = evenBarsAudioFileName
        self.oddBarsLocalReference = oddBarsLocalReference
        self.evenBarsLocalReference = evenBarsLocalReference
    }
    
    init(pieceID: String, dict: [String: Any]) {
        self.pieceID = pieceID
        
        switch DataManager.sharedInstance.userLanguageCode {
        case "en":
            self.instrumentName = dict["instrumentName_en"] as? String
        default:
            self.instrumentName = dict["instrumentName"] as? String
        }

        self.numberOfLoops = dict["numberOfLoops"] as? Int
        self.bucketURL = dict["bucketURL"] as? String
        self.oddBarsAudioFileName = dict["oddBarsAudioFileName"] as? String
        self.evenBarsAudioFileName = dict["evenBarsAudioFileName"] as? String
        
        self.oddBarsLocalReference = pieceID + "_" + oddBarsAudioFileName!
        self.evenBarsLocalReference = pieceID + "_" + evenBarsAudioFileName!
    }

}
