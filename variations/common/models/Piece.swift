//
//  Piece.swift
//  variations
//
//  Created by julien@macmini on 05/12/2016.
//  Copyright © 2016 jbloit. All rights reserved.
//

import Foundation
import Firebase

struct Piece {
    
    let key: String
    var title: String?
    var subtitle: String?
    var composer: String?
    var numberOfBarsInLoop: Int?
    var bpm: Double?
    var timeSignatureNumerator: Int?
    var timeSignatureDenominator: Int?
    var solo: InstrumentPart?
    var continuo: [InstrumentPart]?
    
    let imageBucketURL: String?
    let imageFileName: String?
    let imageLocalReference: String?
    
    let infotextBucketURL: String?
    let infotextFileName: String?
    let infotextLocalReference: String?
    
    let ref: DatabaseReference?
    
    // init from a DB snapshot
    init(snapshot: DataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as? [String: AnyObject]
        title = snapshotValue?["title"] as? String
        subtitle = snapshotValue?["subtitle"] as? String
        composer = snapshotValue?["composer"] as? String
        numberOfBarsInLoop = snapshotValue?["numberOfBarsInLoop"] as? Int
        bpm = snapshotValue?["bpm"] as? Double
        timeSignatureNumerator = snapshotValue?["timeSignatureNumerator"] as? Int
        timeSignatureDenominator = snapshotValue?["timeSignatureDenominator"] as? Int

        solo = InstrumentPart()
        if let soloPartDictionnary = snapshot.childSnapshot(forPath: "solo").value as? [String: AnyObject] {
            solo = InstrumentPart(pieceID:key, dict: soloPartDictionnary)
        }
        
        continuo = []
        let continuoSnapshot = snapshot.childSnapshot(forPath: "continuo")
        for item in continuoSnapshot.children {
            var continuoPart = InstrumentPart()
            let continuoPartSnapshot =  item as! DataSnapshot
            if let continuoPartDictionnary = continuoPartSnapshot.value as? [String: AnyObject]{
                continuoPart = InstrumentPart(pieceID:key, dict: continuoPartDictionnary)
            }
            continuo?.append(continuoPart)
        }

        imageBucketURL = snapshot.childSnapshot(forPath: "image/bucketURL").value as? String
        imageFileName = snapshot.childSnapshot(forPath: "image/filename").value as? String
        imageLocalReference = key + "_" + imageFileName!
        
        infotextBucketURL = snapshot.childSnapshot(forPath: "info/bucketURL").value as? String
        
        switch DataManager.sharedInstance.userLanguageCode {
            case "en":
                infotextFileName = snapshot.childSnapshot(forPath: "info/filename_en").value as? String
        default:
            infotextFileName = snapshot.childSnapshot(forPath: "info/filename").value as? String
        }
        
        infotextLocalReference = key + "_" + infotextFileName!

        ref = snapshot.ref
    }
    
    // return as a dict for DB insert
    //TODO: do we need to complete this? no DB insert will be allowed for Pieces...
    func toAnyObject() -> Any {
        return [
            "title": title,
            "subtitle": subtitle,
            "composer": composer,
        ]
    }
    
}
